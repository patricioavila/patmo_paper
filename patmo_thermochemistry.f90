module patmo_thermochemistry
  use patmo_commons
  use patmo_parameters
  integer,parameter::n_fit_points = 1000
  real*8,parameter::log_tmin=log10(1d0)
  real*8,parameter::log_tmax=log10(1d4)
  real*8::thermofit(chemReactionsNumber, n_fit_points), inv_dxlog

contains

  ! ************************
  ! initialize fits of thermochemical polynomial
  subroutine init_thermochemistry()
    implicit none
    real*8::logTmin, logTmax, Tfit
    real*8::invTgas, invTgas2, lnTgas, Tgas, Tgas2, Tgas3, Tgas4
    integer::i

    ! initialize fit to small value (to avoid log problems)
    thermofit(:, :) = 1d-40

    inv_dxlog = (n_fit_points - 1) / (log_Tmax - log_Tmin)

    ! loop on fit points to evaluate
    do i = 1, n_fit_points
      Tfit = 1d1**((i-1) / inv_dxlog + log_Tmin)
      Tgas = Tfit
      Tgas2 = Tgas * Tgas
      Tgas3 = Tgas2 * Tgas
      Tgas4 = Tgas3 * Tgas
      invTgas = 1d0 / Tgas
      invTgas2 = invTgas**2
      lnTgas = log(Tgas)

    end do

    thermofit(:,:) = log10(thermofit(:,:))

  end subroutine init_thermochemistry

  ! **************************
  function evaluate_thermochemistry_at(Tgas) result(fit)
    implicit none
    real*8,intent(in)::Tgas
    real*8::fit(chemReactionsNumber), x0, Tlog
    integer::idx

    Tlog = log10(Tgas)

    idx = (Tlog - log_Tmin) * inv_dxlog + 1

    x0 = (idx - 1) / inv_dxlog + log_Tmin

    fit(:) = 1d1**((Tlog - x0) *  inv_dxlog &
        * (thermofit(:, idx+1) - thermofit(:, idx)) &
        + thermofit(:, idx))

  end function evaluate_thermochemistry_at

end module patmo_thermochemistry
