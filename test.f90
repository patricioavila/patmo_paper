!this file contains a test for running the model for an FFP's moon atmosphere used in Ávila et al (2021).
program test
  use patmo
  use patmo_commons
  use patmo_constants
  use patmo_parameters
  use patmo_utils
  use patmo_heating
  use patmo_fit
  use patmo_thermo
  use patmo_my_func
  implicit none
  real*8::dt, x(speciesNumber), t, tend, imass
  real*8::pground, n(speciesNumber), ntot,p(41),Temp(41),k(41,41),pmul,Tmul,D,user_CRFlux(cellsNumber)
  integer::icell

  !init all
  call patmo_init()
  call patmo_setGravity(9.8d2)
  call patmo_setEddyKzzAll(1d4)

  x(:) = 0d0
  x(patmo_idx_H2) = 1d-1
  x(patmo_idx_CO2) = 9.0d-1
  x(:) = x(:) / sum(x)
  !init opacity table
  call init_anytab2D("CO2_data",p(:),Temp(:),k(:,:),pmul,Tmul)

  ! pressure at ground, similar to earth
  pground = 1d6 !dyne/cm2, 1d6 =1bar

  dt = 1d-2*secondsPerYear
  tend = 3d8*secondsPerYear
  t = 0d0
 

  D=1.5d0

  Teff = 1.639d2 
  ! compute pressure
  pressure(:)=logspace(pground,1d1,cellsNumber)

  call patmo_computeInitialThermalProfile(D,p,Temp,k,pmul,Tmul,"pascal","m2/kg",x,pground)


  !set cr flux  in every layer, with exponential attenuation
  user_CRflux(:)= exp_flux(1d-25,1.3d-17)
  do icell=1,cellsNumber
    call patmo_setCRflux(icell,user_CRflux(icell))
  end do
 
  !loop on time
  print *, "mass:", patmo_getTotalMass(), " time:", t
  imass = patmo_getTotalMass()
  do
     dt = dt * 1.1
     
     call patmo_run(dt)
     call patmo_dumpMixingRatioToFile(2, t,patmo_idx_H2O)
     call patmo_dumpMixingRatioToFile(8, t,patmo_idx_H2)
     call patmo_dumpMixingRatioToFile(9, t,patmo_idx_CO2)
     call patmo_dumpMixingRatioToFile(10, t,patmo_idx_CO)
     call patmo_dumpMixingRatioToFile(11, t,patmo_idx_O3)
     call patmo_dumpMixingRatioToFile(12, t,patmo_idx_O2)
     call patmo_dumpAllMixingRatioToFile("allMixingRatio.txt")
     t = t + dt
     print '("mass: ",(ES15.6)," | dif_mass: ",(ES15.6)," | time: ",(ES15.6))', patmo_getTotalMass(), abs(patmo_getTotalMass() - imass) / imass, t
     imass = patmo_getTotalMass()
     call patmo_computeThermalProfile(D,p,Temp,k,pmul,Tmul,"pascal","m2/kg",pground)
     if(t >= tend) exit
  end do
  
end program test
