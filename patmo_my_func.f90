module patmo_my_func
contains
  !***************
  function kooij(a,b,c,T)
    implicit none
    real*8::kooij
    real*8,intent(in)::a,b,c,T
    kooij=a*(T/3d2)**b*exp(-c/T)
  end function kooij
  !***************
  function toe1(ao,bo,co,ainf,binf,cinf,T,M)
    implicit none
    real*8::toe1
    real*8,intent(in)::ao,bo,co,ainf,binf,cinf,T,M
    real*8::ko,kinf
    ko=kooij(ao,bo,co,T)
    kinf=kooij(ainf,binf,cinf,T)
    toe1= ko*M/(1d0+ko*M/kinf)
  end function toe1

  function exp_flux(sigma,flux_0)
    use patmo_commons
    use patmo_parameters
    implicit none
    real*8,intent(in)::sigma,flux_0
    real*8::N,ntot(cellsNumber),dz,exp_flux(cellsNumber)
    integer::i

    ntot(:) = sum(nAll(:,1:chemSpeciesNumber),2)
    N = 0d0
    exp_flux(cellsNumber) = flux_0

    do i=cellsNumber-1,1,-1
      dz = height(i+1) - height(i)
      !column density
      N = N + ntot(i+1) * dz
      exp_flux(i) = flux_0 * exp(-sigma*N)
    end do
  end function exp_flux
  !***************
  function linspace(xmin,xmax,n)
    implicit none
    real*8,intent(in)::xmin,xmax
    integer,intent(in)::n
    real*8::linspace(n)
    integer:: i
    if (n == 1) then
      if(xmin /= xmax) then
        write(0,'("ERROR: Cannot call linspace with n=1 and xmin /= xmax")')
        stop
      else
        linspace = xmin
      end if
    else
      do i=1,n
        linspace(i) = (xmax-xmin) * real(i-1) / real(n-1) + xmin
      end do
    end if
  end function linspace

  function logspace(xmin,xmax,n)
    implicit none
    real*8,intent(in) :: xmin,xmax
    integer,intent(in)::n
    real*8::logspace(n)
    logspace = linspace(log10(xmin),log10(xmax),n)
    logspace = 10**logspace
  end function logspace
  !***********
  function ionpol1(a,b,c,T)
    implicit none
    real*8::ionpol1
    real*8,intent(in)::a,b,c,T
    ionpol1 = a * b *(6.2d-1 + 4.767d-1*c*(3d2/T)**5d-1)
  end function ionpol1
  !************
  function ionpol2(a,b,c,T)
    implicit none
    real*8::ionpol2
    real*8,intent(in)::a,b,c,T
    ionpol2 = a * b * (1d0 + 9.67d-2*c*(3d2/T)**5d-1 + (c**2d0)*(3d2/T)/1.0526d1)
  end function ionpol2
  !***************
  function toe2(ao,bo,co,ainf,binf,cinf,ar,br,cr,Temp,M)
    implicit none
    real*8::toe2
    real*8,intent(in)::ao,bo,co,ainf,binf,cinf,ar,br,cr,Temp,M
    real*8::F,ko,kinf,kr
    ko=kooij(ao,bo,co,Temp)
    kinf=kooij(ainf,binf,cinf,Temp)
    kr=kooij(ar,br,cr,Temp)
    F=0.6**(1/(1+(log10(ko*M+kinf))**2))
    toe2=(ko*M*F+kr)*kinf/(ko*M+kinf)
  end function toe2
  !***************
end module patmo_my_func
