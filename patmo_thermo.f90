module patmo_thermo
contains

  ! ********************
  subroutine patmo_computeOpacity(p, T, k_or, pmul, Tmul, Pfactor, Kfactor, icell)
    use patmo_parameters
    use patmo_commons
    use patmo_fit
    implicit none
    real*8,intent(in)::p(:),T(:),k_or(:,:),pmul,Tmul,Pfactor,Kfactor
    real*8::p_in
    integer,intent(in)::icell

    !*********** interpolate opacity over pressure and temperature **********
    !loop on cell
    p_in = pressure(icell) * Pfactor

    k_thermal(icell) = fit_anytab2D(p(:), T(:), k_or(:,:), pmul, Tmul, p_in, TgasAll(icell)) &
        * Kfactor

  end subroutine patmo_computeOpacity

  ! ********************
  subroutine patmo_computeTgas(D,icell)
    use patmo_commons
    use patmo_parameters
    implicit none
    real*8,intent(in)::D
    real*8::Tsup,T4,dp,g
    integer,intent(in)::icell

    g = gravity
    !calculate temperature for icell
    dp = pressure(icell) - pressure(icell+1)

    tauThermalAll(icell) = tauThermalAll(icell+1) + k_thermal(icell+1) * dp / g
    T4 = 5d-1 * (Teff**4) * (D * tauThermalAll(icell) + 1d0)
    TgasAll(icell) = T4**(0.25)

  end subroutine patmo_computeTgas

  ! ***************************
  subroutine patmo_computeThermalProfile(D, p, T, k_or, pmul, Tmul, unitP, unitK,pground)
    use patmo
    use patmo_commons
    use patmo_constants
    use patmo_parameters
    use patmo_utils
    implicit none
    real*8,intent(in)::D,p(:),T(:),k_or(:,:),pmul,Tmul
    real*8::Pfactor,Kfactor,pground
    real*8::dlogT,dlogP,crit,ad,dp,ntot,n(speciesNumber)
    integer::icell
    character(len=*),optional,intent(in)::unitP,unitK

    !converse units if pressure
    if (trim(unitP)=="dyne/cm2") then
      Pfactor=1d0
    else if (trim(unitP)=="dyne") then
      Pfactor=1d0
    else if (trim(unitP)=="bar") then
      Pfactor=1d-6
    else if (trim(unitP)=="mbar") then
      Pfactor=1d-3
    else if (trim(unitP)=="pascal") then
      Pfactor=1d-1
    else if (trim(unitP)=="atm") then
      Pfactor=9.86923d-7
    else
      print *,"ERROR: unknown unit "//trim(unitP)
      stop
    end if

    !converse units of opacity
    if (trim(unitK)=="m2/kg") then
      Kfactor=1d1
    else if (trim(unitK)=="cm2/g") then
      Kfactor=1d0
    else
      print *,"ERROR: unknown unit "//trim(unitK)
      stop
    end if

    tauThermalAll(:) = 0d0
    !temperature on the top
    TgasAll(cellsNumber) = Teff *(5d-1)**(2.5d-1) !<<<<<<<<SURE?

    !opacity on the top
    call patmo_computeOpacity(p,T,k_or,pmul,Tmul,Pfactor,Kfactor,cellsNumber)
    !loop on cell below the top
    ad = (1.3 - 1.0) / 1.3
    do icell=cellsNumber-1,1,-1
      call patmo_computeTgas(D,icell)
      dlogT = log10(TgasAll(icell)) - log10(TgasAll(icell+1))
      dlogP = log10(pressure(icell)) - log10(pressure(icell+1))
      crit = dlogT / dlogP
      !evaluate the convectivity
      if (crit > ad) then
        TgasAll(icell) = TgasAll(icell+1) * (pressure(icell) / pressure(icell+1))**ad
        convectivity(icell) = 1
      else
        convectivity(icell) = 0
      end if
      call patmo_computeOpacity(p, T, k_or, pmul, Tmul, Pfactor, Kfactor, icell)
    end do

    !recalculate the pressure
    call patmo_set_hydrostatic_pressure(pground)
    do icell=1,1
      !temp array
      n(:) = nall(icell,:)
      !total number density p=n*k*T
      ntot = pressure(icell) /kboltzmann /TgasAll(icell)
      !resacale abundances depending on total pressure
      nall(icell,1:chemSpeciesNumber) = &
          n(1:chemSpeciesNumber) &
          / sum(n(1:chemSpeciesNumber))*ntot
    end do

  end subroutine patmo_computeThermalProfile

  ! ***********************
  subroutine patmo_computeInitialThermalProfile(D, p, T, k_or, pmul, Tmul, unitP_in, unitK, x, pground)
    use patmo
    use patmo_commons
    use patmo_constants
    use patmo_parameters
    use patmo_utils
    use patmo_fit
    implicit none
    real*8,intent(in)::D,p(:),T(:),k_or(:,:),pmul,Tmul,pground
    real*8::Pfactor,Kfactor,x(speciesNumber),dz,z(cellsNumber),dp,T_out(cellsNumber),c_out(cellsNumber),P_out(cellsNumber),tau_out(cellsNumber)
    real*8::dlogT,dlogP,crit,ad,n(speciesNumber),ntot,f1,f2,fint
    integer::icell,j
    character(len=*),optional,intent(in)::unitP_in,unitK

    !converse units of pressure
    if (trim(unitP_in)=="dyne/cm2") then
      Pfactor=1d0
    else if (trim(unitP_in)=="dyne") then
      Pfactor=1d0
    else if (trim(unitP_in)=="bar") then
      Pfactor=1d-6
    else if (trim(unitP_in)=="mbar") then
      Pfactor=1d-3
    else if (trim(unitP_in)=="pascal") then
      Pfactor=1d-1
    else if (trim(unitP_in)=="atm") then
      Pfactor=9.86923d-7
    else
      print *,"ERROR: unknown unit "//trim(unitP_in)
      stop
    end if
    !converse units of opacity
    if (trim(unitK)=="m2/kg") then
      Kfactor=1d1
    else if (trim(unitK)=="cm2/g") then
      Kfactor=1d0
    else
      print *,"ERROR: unknown unit "//trim(unitK)
      stop
    end if

    tauThermalAll(:) = 0d0

    !normalize abundances
    x(:) = x(:) / sum(x)

    !temperature on the top
    TgasAll(cellsNumber) = Teff * (5d-1)**0.25

    !densities on the top
    call patmo_setChemistry(cellsNumber, x(:) * pressure(cellsNumber) &
        / kboltzmann / TgasAll(cellsNumber))

    !opacity on the top
    call patmo_computeOpacity(p,T,k_or,pmul,Tmul,Pfactor,Kfactor,cellsNumber)
    !loop on cell below the top
    ad = (1.3 - 1.0) / 1.3
    do icell=cellsNumber-1,1,-1
      call patmo_computeTgas(D,icell)
      dlogT = log10(TgasAll(icell)) - log10(TgasAll(icell+1))
      dlogP = log10(pressure(icell)) - log10(pressure(icell+1))
      crit = dlogT / dlogP
      !evaluate the convectivity
      if (crit > ad) then
        TgasAll(icell) = TgasAll(icell+1) * (pressure(icell) / pressure(icell+1))**ad
        convectivity(icell) = 1
      else
        convectivity(icell) = 0
      end if
      n(:) = x(:) * pressure(icell) / kboltzmann / TgasAll(icell)
      call patmo_setChemistry(icell, n(:))
      call patmo_computeOpacity(p,T,k_or,pmul,Tmul,Pfactor,Kfactor,icell)
    end do

    !loop on cells
    z(:)=0d0
    do icell=2,cellsNumber
      dp = pressure(icell) - pressure(icell-1)
      dz = -dp * kboltzmann * TgasAll(icell) / getMeanMass(nall(icell,:)) &
          / pressure(icell-1) / gravity
      z(icell) = z(icell-1) + dz
    end do

    !interpolate in altitude
    dz = z(cellsNumber) / (cellsNumber - 1)
    do icell=1,cellsNumber
      height(icell) = dz * (icell-1)
      T_out(icell) = fit_anytab1D(z(:), TgasAll(:), 0d0, height(icell))
      c_out(icell) = fit_anytab1D(z(:), dble(convectivity(:)), 0d0, height(icell))
      P_out(icell) = fit_anytab1D(z(:), pressure(:), 0d0, height(icell))
      tau_out(icell) = fit_anytab1D(z(:), tauThermalAll(:), 0d0, height(icell))
    end do

    TgasAll(:) = T_out(:)
    convectivity(:) = idnint(c_out(:))
    pressure(:) = P_out(:)
    tauThermalAll(:) = tau_out(:)

    do icell=1,cellsNumber
      !temp array
      n(:) = nall(icell, :)
      !total number density p=n*k*T
      ntot = pressure(icell) / kboltzmann / TgasAll(icell)
      !rescale abundances depending on total pressure
      nall(icell, 1:chemSpeciesNumber) = &
          n(1:chemSpeciesNumber) &
          / sum(n(1:chemSpeciesNumber)) * ntot
    end do

  end subroutine patmo_computeInitialThermalProfile

end module patmo_thermo
