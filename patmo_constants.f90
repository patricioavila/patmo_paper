module patmo_constants
  implicit none
  !boltzmann constant, erg/K
  real*8,parameter::kboltzmann = 1.38064852d-16
  !boltzmann constant, eV/K
  real*8,parameter::kboltzmann_eV = 8.617332478d-5
  !seconds per year, s
  real*8,parameter::secondsPerYear = 365d0*24d0*3600d0
  !pi
  real*8,parameter::pi = 4d0*atan(1d0)
  !speed of light, cm/s
  real*8,parameter::clight = 2.99792458d10
  !plank, eV*s
  real*8,parameter::planck_eV = 4.135667516d-15
  !gravitational constant, cm^3 g^-1 s^-2
  real*8,parameter::G=6.67408d-8
  !stefan-boltzmann constant, erg cm^−2 s^−1 K^−4.
  real*8,parameter::sb=5.67037d-5
  !earth's radius, cm
  real*8,parameter::r_earth = 6.371d8
  !earth's density, g cm^-3
  real*8,parameter::rho_earth = 5.514d0
  !earth's mass, g
  real*8,parameter::m_earth = 5.97237d27
  !jupiter's mass, g
  real*8,parameter::m_jupiter = 1.8982d30
  !jupiter's radius, cm
  real*8,parameter::r_jupiter = 6.9911d9
  !Astronomical Unit, cm
  real*8,parameter::AU = 1.496d13
end module patmo_constants
