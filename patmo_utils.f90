module patmo_utils
contains

  !NOTE
  !references for some of these functions are in
  !https://bitbucket.org/tgrassi/planatmo/wiki/user_functions

  !**************
  subroutine computeEntropyProductionFlux(dt)
    use patmo_commons
    use patmo_parameters
    implicit none
    real*8,intent(in)::dt
    real*8::flux(reactionsNumber)
    integer::icell

    !loop on cells
    do icell=1,cellsNumber
      !get reaction fluxes
      flux(:) = getFlux(nAll(:,:),icell)
      cumulativeFlux(icell,:) = &
          cumulativeFlux(icell,:) + flux(:) * dt
    end do

  end subroutine computeEntropyProductionFlux

  !*************
  function getEntropyProduction(timeInterval)
    use patmo_commons
    use patmo_parameters
    implicit none
    real*8,intent(in)::timeInterval
    real*8::getEntropyProduction,ds,dsp,ratio
    integer::i,icell,lowReverse(chemReactionsNumber)

    !small Reverse/Forward ratio flags (to rise warning message)
    lowReverse(:) = 0

    open(22,file="entropyReport.out",status="replace")
    write(22,*) "#cell_number  reaction_index   contribution_to_entropy   Rb/Rf"
    ds = 0d0
    !loop on cells
    do icell=1,cellsNumber
      !get reaction fluxes
      do i=1,chemReactionsNumber
        !check if Reverse/Forward ratio is small
        ratio = cumulativeFlux(icell,reverseReactionsOffset+i) &
            / cumulativeFlux(icell,i)
        if(ratio<1d-10) then
          lowReverse(i) = 1
        end if
        !sum entropy production
        dsp = (cumulativeFlux(icell,i) &
            - cumulativeFlux(icell,reverseReactionsOffset+i)) &
            * log(cumulativeFlux(icell,i) &
            / cumulativeFlux(icell,reverseReactionsOffset+i))
        ds = ds + dsp
        write(22,'(2I5,2E17.8e3)') icell,i,dsp/timeInterval,ratio
      end do
      write(22,*)
    end do
    close(22)

    !rise warning if Reverse/Forward ratio found
    if(sum(lowReverse)>0) then
      print *,"WARNING: suspiciously low Reverse/Forward flux ratio for rates:"
      do i=1,chemReactionsNumber
        if(lowReverse(i)==1) print *,i
      end do
      print *,"check entropyReport.out file"
    end if

    !divide by total time
    getEntropyProduction = ds / timeInterval

  end function getEntropyProduction

  !**************
  !get the best nbest reaction fluxes indexes
  function getBestFluxIdx(icell, nbest)
    use patmo_commons
    use patmo_parameters
    implicit none
    real*8::flux(reactionsNumber),maxFlux
    integer,intent(in)::icell,nbest
    integer::i,j,idxList(nbest),getBestFluxIdx(nbest)

    !get fluxes
    flux(:) = getFlux(nAll(:,:), icell)

    !loop on number of best flux required
    do j=1, nbest
      maxFlux = 0d0
      !loop on reactions to find max
      do i=1, reactionsNumber
        if(flux(i) .ge. maxFlux) then
          idxList(j) = i
          maxFlux = flux(i)
        end if
      end do
      !set max flux to zero to find next best
      flux(idxList(j)) = 0d0
    end do

    getBestFluxIdx(:) = idxList(:)

  end function getBestFluxIdx

  !**************
  !get reaction fluxes for cell icell
  function getFlux(nin,icell)
    use patmo_commons
    use patmo_parameters
    implicit none
    real*8,intent(in)::nin(cellsNumber,speciesNumber)
    integer,intent(in)::icell
    integer::i
    real*8::n(speciesNumber)
    real*8::getFlux(reactionsNumber)

    !local copy for the given cell
    n(:) = nin(icell,:)
    n(positionDummy) = 1d0

    !loop on reactions
    do i=1,reactionsNumber
      getFlux(i) = krate(icell,i) &
          * n(indexReactants1(i)) &
          * n(indexReactants2(i))
    end do

  end function getFlux

  !***************
  !get the degree weighted by flux cut at a fraction
  ! of the maximum
  function getDegreeFluxWeighted(n,icell,fraction)
    use patmo_commons
    implicit none
    real*8,intent(in)::n(cellsNumber,speciesNumber),fraction
    integer,intent(in)::icell
    integer::degree(speciesNumber),i
    integer::getDegreeFluxWeighted(speciesNumber)
    real*8::flux(reactionsNumber),fluxFrac

    !get flux
    flux(:) = getFlux(n(:,:),icell)
    !fraction of the maximum flux
    fluxFrac = maxval(flux(:))*fraction

    degree(:) = 0
    do i=1,reactionsNumber
      if(flux(i)>=fluxFrac) then
        degree(indexReactants1(i)) = degree(indexReactants1(i)) + 1
        degree(indexReactants2(i)) = degree(indexReactants2(i)) + 1
        degree(indexProducts1(i)) = degree(indexProducts1(i)) + 1
        degree(indexProducts2(i)) = degree(indexProducts2(i)) + 1
        degree(indexProducts3(i)) = degree(indexProducts3(i)) + 1

      end if
    end do

    getDegreeFluxWeighted(:) = degree(:)

  end function getDegreeFluxWeighted

  !********************
  function getDegreeHistogram(n,icell)
    use patmo_commons
    implicit none
    real*8,intent(in)::n(cellsNumber,speciesNumber)
    integer,intent(in)::icell
    integer::i,degree(speciesNumber)
    integer::hist(speciesNumber)
    integer::getDegreeHistogram(speciesNumber)
    real*8::fraction

    fraction = 1d-1

    degree(:) = getDegreeFluxWeighted(n(:,:),icell,fraction)
    hist(:) = 0
    do i=1,speciesNumber
      hist(degree(i)) = hist(degree(i)) + 1
    end do

    getDegreeHistogram(:) = hist(:)

  end function getDegreeHistogram

  !**************
  !get an array with all the masses
  function getSpeciesMass()
    use patmo_commons
    implicit none
    real*8::getspeciesMass(speciesNumber)

    getSpeciesMass(patmo_idx_Oj) = 2.678677d-23
    getSpeciesMass(patmo_idx_CH5j) = 2.845752d-23
    getSpeciesMass(patmo_idx_CO2j) = 7.366522d-23
    getSpeciesMass(patmo_idx_CH3OH) = 5.357258d-23
    getSpeciesMass(patmo_idx_C2H5Oj) = 7.533596d-23
    getSpeciesMass(patmo_idx_C2H4O) = 7.366334d-23
    getSpeciesMass(patmo_idx_O3) = 8.036305d-23
    getSpeciesMass(patmo_idx_O2) = 5.357536d-23
    getSpeciesMass(patmo_idx_CHj) = 2.176338d-23
    getSpeciesMass(patmo_idx_CH2) = 2.343783d-23
    getSpeciesMass(patmo_idx_CH3) = 2.511136d-23
    getSpeciesMass(patmo_idx_CH4) = 2.678489d-23
    getSpeciesMass(patmo_idx_O2j) = 5.357445d-23
    getSpeciesMass(patmo_idx_C2H3Oj) = 7.198889d-23
    getSpeciesMass(patmo_idx_C2H) = 4.185506d-23
    getSpeciesMass(patmo_idx_C2O) = 6.696921d-23
    getSpeciesMass(patmo_idx_H) = 1.673533d-24
    getSpeciesMass(patmo_idx_CH4j) = 2.678398d-23
    getSpeciesMass(patmo_idx_H2O2j) = 5.692152d-23
    getSpeciesMass(patmo_idx_C2j) = 4.018061d-23
    getSpeciesMass(patmo_idx_C4H) = 8.203658d-23
    getSpeciesMass(patmo_idx_CH3O2) = 7.868673d-23
    getSpeciesMass(patmo_idx_CH3CH2O) = 7.533687d-23
    getSpeciesMass(patmo_idx_C2H5OH) = 7.70104d-23
    getSpeciesMass(patmo_idx_HCOOH) = 7.701319d-23
    getSpeciesMass(patmo_idx_C2H4j) = 4.687474d-23
    getSpeciesMass(patmo_idx_C2H7Oj) = 7.868303d-23
    getSpeciesMass(patmo_idx_CH3CHOH) = 7.533687d-23
    getSpeciesMass(patmo_idx_C2H2O) = 7.031627d-23
    getSpeciesMass(patmo_idx_C3H4) = 6.696642d-23
    getSpeciesMass(patmo_idx_C3H5) = 6.863995d-23
    getSpeciesMass(patmo_idx_C3H3) = 6.529288d-23
    getSpeciesMass(patmo_idx_HOj) = 2.84603d-23
    getSpeciesMass(patmo_idx_C) = 2.009076d-23
    getSpeciesMass(patmo_idx_HO) = 2.846122d-23
    getSpeciesMass(patmo_idx_CH2O) = 5.022551d-23
    getSpeciesMass(patmo_idx_CH2OH) = 5.189904d-23
    getSpeciesMass(patmo_idx_C2H4Oj) = 7.366243d-23
    getSpeciesMass(patmo_idx_C2H2Oj) = 7.031536d-23
    getSpeciesMass(patmo_idx_H2O2) = 5.692243d-23
    getSpeciesMass(patmo_idx_C2H3j) = 4.520121d-23
    getSpeciesMass(patmo_idx_CH5Oj) = 5.52452d-23
    getSpeciesMass(patmo_idx_CH3Oj) = 5.189813d-23
    getSpeciesMass(patmo_idx_H2Oj) = 3.013384d-23
    getSpeciesMass(patmo_idx_HCCO) = 6.864274d-23
    getSpeciesMass(patmo_idx_CH3j) = 2.511045d-23
    getSpeciesMass(patmo_idx_ew) = 9.109384d-28
    getSpeciesMass(patmo_idx_CH3O2j) = 7.868581d-23
    getSpeciesMass(patmo_idx_C2H3O) = 7.19898d-23
    getSpeciesMass(patmo_idx_CHOj) = 4.855107d-23
    getSpeciesMass(patmo_idx_C2Hj) = 4.185415d-23
    getSpeciesMass(patmo_idx_H3Oj) = 3.180737d-23
    getSpeciesMass(patmo_idx_CH4O2) = 8.036026d-23
    getSpeciesMass(patmo_idx_HO2j) = 5.524799d-23
    getSpeciesMass(patmo_idx_O) = 2.678768d-23
    getSpeciesMass(patmo_idx_CH4Oj) = 5.357166d-23
    getSpeciesMass(patmo_idx_C2H6) = 5.022272d-23
    getSpeciesMass(patmo_idx_C2H5) = 4.854919d-23
    getSpeciesMass(patmo_idx_CH3O) = 5.189904d-23
    getSpeciesMass(patmo_idx_C2H3) = 4.520212d-23
    getSpeciesMass(patmo_idx_C2H2) = 4.352859d-23
    getSpeciesMass(patmo_idx_CH2O2) = 7.701319d-23
    getSpeciesMass(patmo_idx_C2H2j) = 4.352768d-23
    getSpeciesMass(patmo_idx_H2) = 3.347066d-24
    getSpeciesMass(patmo_idx_Hj) = 1.672622d-24
    getSpeciesMass(patmo_idx_CH2Oj) = 5.02246d-23
    getSpeciesMass(patmo_idx_CH2j) = 2.343692d-23
    getSpeciesMass(patmo_idx_C4H2j) = 8.37092d-23
    getSpeciesMass(patmo_idx_H2j) = 3.346155d-24
    getSpeciesMass(patmo_idx_Cj) = 2.008985d-23
    getSpeciesMass(patmo_idx_C2H4) = 4.687566d-23
    getSpeciesMass(patmo_idx_CHO2j) = 7.533875d-23
    getSpeciesMass(patmo_idx_C2H5j) = 4.854828d-23
    getSpeciesMass(patmo_idx_C2H3O_c) = 7.19898d-23
    getSpeciesMass(patmo_idx_C3H4j) = 6.696551d-23
    getSpeciesMass(patmo_idx_CH) = 2.176429d-23
    getSpeciesMass(patmo_idx_CO) = 4.687844d-23
    getSpeciesMass(patmo_idx_C2) = 4.018152d-23
    getSpeciesMass(patmo_idx_CH3COCH3) = 9.710117d-23
    getSpeciesMass(patmo_idx_CO2) = 7.366613d-23
    getSpeciesMass(patmo_idx_H2O) = 3.013475d-23
    getSpeciesMass(patmo_idx_C4H5j) = 8.87298d-23
    getSpeciesMass(patmo_idx_COj) = 4.687753d-23
    getSpeciesMass(patmo_idx_C3H3O) = 9.208057d-23
    getSpeciesMass(patmo_idx_CH2CHO) = 7.19898d-23
    getSpeciesMass(patmo_idx_H3j) = 5.019688d-24
    getSpeciesMass(patmo_idx_C3H7Oj) = 9.877379d-23
    getSpeciesMass(patmo_idx_C3H2j) = 6.361844d-23
    getSpeciesMass(patmo_idx_HO2) = 5.52489d-23
    getSpeciesMass(patmo_idx_C2H7j) = 5.189534d-23
    getSpeciesMass(patmo_idx_C4H3) = 8.538365d-23
    getSpeciesMass(patmo_idx_C4H2) = 8.371011d-23
    getSpeciesMass(patmo_idx_C4H4) = 8.705718d-23
    getSpeciesMass(patmo_idx_CHO) = 4.855198d-23
    getSpeciesMass(patmo_idx_C3H3j) = 6.529197d-23
    getSpeciesMass(patmo_idx_COOH) = 7.533966d-23
    getSpeciesMass(patmo_idx_C2HOj) = 6.864183d-23
    getSpeciesMass(patmo_idx_C2H6j) = 5.022181d-23
    getSpeciesMass(patmo_idx_He) = 6.696921d-24
    getSpeciesMass(patmo_idx_Ar) = 6.027229d-23
    getSpeciesMass(patmo_idx_Kr) = 1.205446d-22

  end function getSpeciesMass

  !*****************
  !get an array with all the species names
  function getSpeciesNames()
    use patmo_commons
    implicit none
    character(len=maxNameLength)::getSpeciesNames(speciesNumber)

    getSpeciesNames(patmo_idx_Oj) = "O+"
    getSpeciesNames(patmo_idx_CH5j) = "CH5+"
    getSpeciesNames(patmo_idx_CO2j) = "CO2+"
    getSpeciesNames(patmo_idx_CH3OH) = "CH3OH"
    getSpeciesNames(patmo_idx_C2H5Oj) = "C2H5O+"
    getSpeciesNames(patmo_idx_C2H4O) = "C2H4O"
    getSpeciesNames(patmo_idx_O3) = "O3"
    getSpeciesNames(patmo_idx_O2) = "O2"
    getSpeciesNames(patmo_idx_CHj) = "CH+"
    getSpeciesNames(patmo_idx_CH2) = "CH2"
    getSpeciesNames(patmo_idx_CH3) = "CH3"
    getSpeciesNames(patmo_idx_CH4) = "CH4"
    getSpeciesNames(patmo_idx_O2j) = "O2+"
    getSpeciesNames(patmo_idx_C2H3Oj) = "C2H3O+"
    getSpeciesNames(patmo_idx_C2H) = "C2H"
    getSpeciesNames(patmo_idx_C2O) = "C2O"
    getSpeciesNames(patmo_idx_H) = "H"
    getSpeciesNames(patmo_idx_CH4j) = "CH4+"
    getSpeciesNames(patmo_idx_H2O2j) = "H2O2+"
    getSpeciesNames(patmo_idx_C2j) = "C2+"
    getSpeciesNames(patmo_idx_C4H) = "C4H"
    getSpeciesNames(patmo_idx_CH3O2) = "CH3O2"
    getSpeciesNames(patmo_idx_CH3CH2O) = "CH3CH2O"
    getSpeciesNames(patmo_idx_C2H5OH) = "C2H5OH"
    getSpeciesNames(patmo_idx_HCOOH) = "HCOOH"
    getSpeciesNames(patmo_idx_C2H4j) = "C2H4+"
    getSpeciesNames(patmo_idx_C2H7Oj) = "C2H7O+"
    getSpeciesNames(patmo_idx_CH3CHOH) = "CH3CHOH"
    getSpeciesNames(patmo_idx_C2H2O) = "C2H2O"
    getSpeciesNames(patmo_idx_C3H4) = "C3H4"
    getSpeciesNames(patmo_idx_C3H5) = "C3H5"
    getSpeciesNames(patmo_idx_C3H3) = "C3H3"
    getSpeciesNames(patmo_idx_HOj) = "HO+"
    getSpeciesNames(patmo_idx_C) = "C"
    getSpeciesNames(patmo_idx_HO) = "HO"
    getSpeciesNames(patmo_idx_CH2O) = "CH2O"
    getSpeciesNames(patmo_idx_CH2OH) = "CH2OH"
    getSpeciesNames(patmo_idx_C2H4Oj) = "C2H4O+"
    getSpeciesNames(patmo_idx_C2H2Oj) = "C2H2O+"
    getSpeciesNames(patmo_idx_H2O2) = "H2O2"
    getSpeciesNames(patmo_idx_C2H3j) = "C2H3+"
    getSpeciesNames(patmo_idx_CH5Oj) = "CH5O+"
    getSpeciesNames(patmo_idx_CH3Oj) = "CH3O+"
    getSpeciesNames(patmo_idx_H2Oj) = "H2O+"
    getSpeciesNames(patmo_idx_HCCO) = "HCCO"
    getSpeciesNames(patmo_idx_CH3j) = "CH3+"
    getSpeciesNames(patmo_idx_ew) = "e-"
    getSpeciesNames(patmo_idx_CH3O2j) = "CH3O2+"
    getSpeciesNames(patmo_idx_C2H3O) = "C2H3O"
    getSpeciesNames(patmo_idx_CHOj) = "CHO+"
    getSpeciesNames(patmo_idx_C2Hj) = "C2H+"
    getSpeciesNames(patmo_idx_H3Oj) = "H3O+"
    getSpeciesNames(patmo_idx_CH4O2) = "CH4O2"
    getSpeciesNames(patmo_idx_HO2j) = "HO2+"
    getSpeciesNames(patmo_idx_O) = "O"
    getSpeciesNames(patmo_idx_CH4Oj) = "CH4O+"
    getSpeciesNames(patmo_idx_C2H6) = "C2H6"
    getSpeciesNames(patmo_idx_C2H5) = "C2H5"
    getSpeciesNames(patmo_idx_CH3O) = "CH3O"
    getSpeciesNames(patmo_idx_C2H3) = "C2H3"
    getSpeciesNames(patmo_idx_C2H2) = "C2H2"
    getSpeciesNames(patmo_idx_CH2O2) = "CH2O2"
    getSpeciesNames(patmo_idx_C2H2j) = "C2H2+"
    getSpeciesNames(patmo_idx_H2) = "H2"
    getSpeciesNames(patmo_idx_Hj) = "H+"
    getSpeciesNames(patmo_idx_CH2Oj) = "CH2O+"
    getSpeciesNames(patmo_idx_CH2j) = "CH2+"
    getSpeciesNames(patmo_idx_C4H2j) = "C4H2+"
    getSpeciesNames(patmo_idx_H2j) = "H2+"
    getSpeciesNames(patmo_idx_Cj) = "C+"
    getSpeciesNames(patmo_idx_C2H4) = "C2H4"
    getSpeciesNames(patmo_idx_CHO2j) = "CHO2+"
    getSpeciesNames(patmo_idx_C2H5j) = "C2H5+"
    getSpeciesNames(patmo_idx_C2H3O_c) = "C2H3O_c"
    getSpeciesNames(patmo_idx_C3H4j) = "C3H4+"
    getSpeciesNames(patmo_idx_CH) = "CH"
    getSpeciesNames(patmo_idx_CO) = "CO"
    getSpeciesNames(patmo_idx_C2) = "C2"
    getSpeciesNames(patmo_idx_CH3COCH3) = "CH3COCH3"
    getSpeciesNames(patmo_idx_CO2) = "CO2"
    getSpeciesNames(patmo_idx_H2O) = "H2O"
    getSpeciesNames(patmo_idx_C4H5j) = "C4H5+"
    getSpeciesNames(patmo_idx_COj) = "CO+"
    getSpeciesNames(patmo_idx_C3H3O) = "C3H3O"
    getSpeciesNames(patmo_idx_CH2CHO) = "CH2CHO"
    getSpeciesNames(patmo_idx_H3j) = "H3+"
    getSpeciesNames(patmo_idx_C3H7Oj) = "C3H7O+"
    getSpeciesNames(patmo_idx_C3H2j) = "C3H2+"
    getSpeciesNames(patmo_idx_HO2) = "HO2"
    getSpeciesNames(patmo_idx_C2H7j) = "C2H7+"
    getSpeciesNames(patmo_idx_C4H3) = "C4H3"
    getSpeciesNames(patmo_idx_C4H2) = "C4H2"
    getSpeciesNames(patmo_idx_C4H4) = "C4H4"
    getSpeciesNames(patmo_idx_CHO) = "CHO"
    getSpeciesNames(patmo_idx_C3H3j) = "C3H3+"
    getSpeciesNames(patmo_idx_COOH) = "COOH"
    getSpeciesNames(patmo_idx_C2HOj) = "C2HO+"
    getSpeciesNames(patmo_idx_C2H6j) = "C2H6+"
    getSpeciesNames(patmo_idx_He) = "He"
    getSpeciesNames(patmo_idx_Ar) = "Ar"
    getSpeciesNames(patmo_idx_Kr) = "Kr"

  end function getSpeciesNames

  !***************************
  function getTotalMassNuclei_C()
    use patmo_commons
    use patmo_parameters
    implicit none
    integer::icell
    real*8::getTotalMassNuclei_C
    real*8::m(speciesNumber)

    m(:) = getSpeciesMass()

    getTotalMassNuclei_C = 0d0

    do icell=1,cellsNumber
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH5j) * nall(icell,patmo_idx_CH5j)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CO2j) * nall(icell,patmo_idx_CO2j)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3OH) * nall(icell,patmo_idx_CH3OH)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H5Oj) * nall(icell,patmo_idx_C2H5Oj) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H4O) * nall(icell,patmo_idx_C2H4O) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CHj) * nall(icell,patmo_idx_CHj)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH2) * nall(icell,patmo_idx_CH2)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3) * nall(icell,patmo_idx_CH3)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH4) * nall(icell,patmo_idx_CH4)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H3Oj) * nall(icell,patmo_idx_C2H3Oj) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H) * nall(icell,patmo_idx_C2H) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2O) * nall(icell,patmo_idx_C2O) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH4j) * nall(icell,patmo_idx_CH4j)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2j) * nall(icell,patmo_idx_C2j) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C4H) * nall(icell,patmo_idx_C4H) * 4d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3O2) * nall(icell,patmo_idx_CH3O2)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3CH2O) * nall(icell,patmo_idx_CH3CH2O)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H5OH) * nall(icell,patmo_idx_C2H5OH) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_HCOOH) * nall(icell,patmo_idx_HCOOH)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H4j) * nall(icell,patmo_idx_C2H4j) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H7Oj) * nall(icell,patmo_idx_C2H7Oj) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3CHOH) * nall(icell,patmo_idx_CH3CHOH)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H2O) * nall(icell,patmo_idx_C2H2O) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C3H4) * nall(icell,patmo_idx_C3H4) * 3d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C3H5) * nall(icell,patmo_idx_C3H5) * 3d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C3H3) * nall(icell,patmo_idx_C3H3) * 3d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C) * nall(icell,patmo_idx_C)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH2O) * nall(icell,patmo_idx_CH2O)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH2OH) * nall(icell,patmo_idx_CH2OH)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H4Oj) * nall(icell,patmo_idx_C2H4Oj) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H2Oj) * nall(icell,patmo_idx_C2H2Oj) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H3j) * nall(icell,patmo_idx_C2H3j) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH5Oj) * nall(icell,patmo_idx_CH5Oj)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3Oj) * nall(icell,patmo_idx_CH3Oj)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_HCCO) * nall(icell,patmo_idx_HCCO)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3j) * nall(icell,patmo_idx_CH3j)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3O2j) * nall(icell,patmo_idx_CH3O2j)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H3O) * nall(icell,patmo_idx_C2H3O) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CHOj) * nall(icell,patmo_idx_CHOj)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2Hj) * nall(icell,patmo_idx_C2Hj) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH4O2) * nall(icell,patmo_idx_CH4O2)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH4Oj) * nall(icell,patmo_idx_CH4Oj)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H6) * nall(icell,patmo_idx_C2H6) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H5) * nall(icell,patmo_idx_C2H5) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3O) * nall(icell,patmo_idx_CH3O)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H3) * nall(icell,patmo_idx_C2H3) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H2) * nall(icell,patmo_idx_C2H2) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH2O2) * nall(icell,patmo_idx_CH2O2)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H2j) * nall(icell,patmo_idx_C2H2j) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH2Oj) * nall(icell,patmo_idx_CH2Oj)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH2j) * nall(icell,patmo_idx_CH2j)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C4H2j) * nall(icell,patmo_idx_C4H2j) * 4d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_Cj) * nall(icell,patmo_idx_Cj)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H4) * nall(icell,patmo_idx_C2H4) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CHO2j) * nall(icell,patmo_idx_CHO2j)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H5j) * nall(icell,patmo_idx_C2H5j) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H3O_c) * nall(icell,patmo_idx_C2H3O_c) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C3H4j) * nall(icell,patmo_idx_C3H4j) * 3d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH) * nall(icell,patmo_idx_CH)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CO) * nall(icell,patmo_idx_CO)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2) * nall(icell,patmo_idx_C2) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH3COCH3) * nall(icell,patmo_idx_CH3COCH3)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CO2) * nall(icell,patmo_idx_CO2)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C4H5j) * nall(icell,patmo_idx_C4H5j) * 4d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_COj) * nall(icell,patmo_idx_COj)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C3H3O) * nall(icell,patmo_idx_C3H3O) * 3d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CH2CHO) * nall(icell,patmo_idx_CH2CHO)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C3H7Oj) * nall(icell,patmo_idx_C3H7Oj) * 3d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C3H2j) * nall(icell,patmo_idx_C3H2j) * 3d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H7j) * nall(icell,patmo_idx_C2H7j) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C4H3) * nall(icell,patmo_idx_C4H3) * 4d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C4H2) * nall(icell,patmo_idx_C4H2) * 4d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C4H4) * nall(icell,patmo_idx_C4H4) * 4d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_CHO) * nall(icell,patmo_idx_CHO)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C3H3j) * nall(icell,patmo_idx_C3H3j) * 3d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_COOH) * nall(icell,patmo_idx_COOH)
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2HOj) * nall(icell,patmo_idx_C2HOj) * 2d0
      getTotalMassNuclei_C = getTotalMassNuclei_C + m(patmo_idx_C2H6j) * nall(icell,patmo_idx_C2H6j) * 2d0
    end do

  end function

  !***************************
  function getTotalMassNuclei_e()
    use patmo_commons
    use patmo_parameters
    implicit none
    integer::icell
    real*8::getTotalMassNuclei_e
    real*8::m(speciesNumber)

    m(:) = getSpeciesMass()

    getTotalMassNuclei_e = 0d0

    do icell=1,cellsNumber
      getTotalMassNuclei_e = getTotalMassNuclei_e + m(patmo_idx_ew) * nall(icell,patmo_idx_ew)
    end do

  end function

  !***************************
  function getTotalMassNuclei_H()
    use patmo_commons
    use patmo_parameters
    implicit none
    integer::icell
    real*8::getTotalMassNuclei_H
    real*8::m(speciesNumber)

    m(:) = getSpeciesMass()

    getTotalMassNuclei_H = 0d0

    do icell=1,cellsNumber
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH5j) * nall(icell,patmo_idx_CH5j) * 5d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3OH) * nall(icell,patmo_idx_CH3OH)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H5Oj) * nall(icell,patmo_idx_C2H5Oj) * 5d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H4O) * nall(icell,patmo_idx_C2H4O) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CHj) * nall(icell,patmo_idx_CHj)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH2) * nall(icell,patmo_idx_CH2) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3) * nall(icell,patmo_idx_CH3) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH4) * nall(icell,patmo_idx_CH4) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H3Oj) * nall(icell,patmo_idx_C2H3Oj) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H) * nall(icell,patmo_idx_C2H)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H) * nall(icell,patmo_idx_H)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH4j) * nall(icell,patmo_idx_CH4j) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H2O2j) * nall(icell,patmo_idx_H2O2j) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C4H) * nall(icell,patmo_idx_C4H)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3O2) * nall(icell,patmo_idx_CH3O2) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3CH2O) * nall(icell,patmo_idx_CH3CH2O) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H5OH) * nall(icell,patmo_idx_C2H5OH)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_HCOOH) * nall(icell,patmo_idx_HCOOH)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H4j) * nall(icell,patmo_idx_C2H4j) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H7Oj) * nall(icell,patmo_idx_C2H7Oj) * 7d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3CHOH) * nall(icell,patmo_idx_CH3CHOH)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H2O) * nall(icell,patmo_idx_C2H2O) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C3H4) * nall(icell,patmo_idx_C3H4) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C3H5) * nall(icell,patmo_idx_C3H5) * 5d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C3H3) * nall(icell,patmo_idx_C3H3) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_HOj) * nall(icell,patmo_idx_HOj)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_HO) * nall(icell,patmo_idx_HO)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH2O) * nall(icell,patmo_idx_CH2O) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH2OH) * nall(icell,patmo_idx_CH2OH)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H4Oj) * nall(icell,patmo_idx_C2H4Oj) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H2Oj) * nall(icell,patmo_idx_C2H2Oj) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H2O2) * nall(icell,patmo_idx_H2O2) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H3j) * nall(icell,patmo_idx_C2H3j) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH5Oj) * nall(icell,patmo_idx_CH5Oj) * 5d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3Oj) * nall(icell,patmo_idx_CH3Oj) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H2Oj) * nall(icell,patmo_idx_H2Oj) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_HCCO) * nall(icell,patmo_idx_HCCO)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3j) * nall(icell,patmo_idx_CH3j) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3O2j) * nall(icell,patmo_idx_CH3O2j) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H3O) * nall(icell,patmo_idx_C2H3O) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CHOj) * nall(icell,patmo_idx_CHOj)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2Hj) * nall(icell,patmo_idx_C2Hj)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H3Oj) * nall(icell,patmo_idx_H3Oj) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH4O2) * nall(icell,patmo_idx_CH4O2) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_HO2j) * nall(icell,patmo_idx_HO2j)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH4Oj) * nall(icell,patmo_idx_CH4Oj) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H6) * nall(icell,patmo_idx_C2H6) * 6d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H5) * nall(icell,patmo_idx_C2H5) * 5d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3O) * nall(icell,patmo_idx_CH3O) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H3) * nall(icell,patmo_idx_C2H3) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H2) * nall(icell,patmo_idx_C2H2) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH2O2) * nall(icell,patmo_idx_CH2O2) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H2j) * nall(icell,patmo_idx_C2H2j) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H2) * nall(icell,patmo_idx_H2) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_Hj) * nall(icell,patmo_idx_Hj)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH2Oj) * nall(icell,patmo_idx_CH2Oj) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH2j) * nall(icell,patmo_idx_CH2j) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C4H2j) * nall(icell,patmo_idx_C4H2j) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H2j) * nall(icell,patmo_idx_H2j) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H4) * nall(icell,patmo_idx_C2H4) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CHO2j) * nall(icell,patmo_idx_CHO2j)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H5j) * nall(icell,patmo_idx_C2H5j) * 5d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H3O_c) * nall(icell,patmo_idx_C2H3O_c) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C3H4j) * nall(icell,patmo_idx_C3H4j) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH) * nall(icell,patmo_idx_CH)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH3COCH3) * nall(icell,patmo_idx_CH3COCH3) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H2O) * nall(icell,patmo_idx_H2O) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C4H5j) * nall(icell,patmo_idx_C4H5j) * 5d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C3H3O) * nall(icell,patmo_idx_C3H3O) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CH2CHO) * nall(icell,patmo_idx_CH2CHO)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_H3j) * nall(icell,patmo_idx_H3j) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C3H7Oj) * nall(icell,patmo_idx_C3H7Oj) * 7d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C3H2j) * nall(icell,patmo_idx_C3H2j) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_HO2) * nall(icell,patmo_idx_HO2)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H7j) * nall(icell,patmo_idx_C2H7j) * 7d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C4H3) * nall(icell,patmo_idx_C4H3) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C4H2) * nall(icell,patmo_idx_C4H2) * 2d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C4H4) * nall(icell,patmo_idx_C4H4) * 4d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_CHO) * nall(icell,patmo_idx_CHO)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C3H3j) * nall(icell,patmo_idx_C3H3j) * 3d0
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_COOH) * nall(icell,patmo_idx_COOH)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2HOj) * nall(icell,patmo_idx_C2HOj)
      getTotalMassNuclei_H = getTotalMassNuclei_H + m(patmo_idx_C2H6j) * nall(icell,patmo_idx_C2H6j) * 6d0
    end do

  end function

  !***************************
  function getTotalMassNuclei_O()
    use patmo_commons
    use patmo_parameters
    implicit none
    integer::icell
    real*8::getTotalMassNuclei_O
    real*8::m(speciesNumber)

    m(:) = getSpeciesMass()

    getTotalMassNuclei_O = 0d0

    do icell=1,cellsNumber
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_Oj) * nall(icell,patmo_idx_Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CO2j) * nall(icell,patmo_idx_CO2j) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH3OH) * nall(icell,patmo_idx_CH3OH)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H5Oj) * nall(icell,patmo_idx_C2H5Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H4O) * nall(icell,patmo_idx_C2H4O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_O3) * nall(icell,patmo_idx_O3) * 3d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_O2) * nall(icell,patmo_idx_O2) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_O2j) * nall(icell,patmo_idx_O2j) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H3Oj) * nall(icell,patmo_idx_C2H3Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2O) * nall(icell,patmo_idx_C2O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_H2O2j) * nall(icell,patmo_idx_H2O2j) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH3O2) * nall(icell,patmo_idx_CH3O2) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH3CH2O) * nall(icell,patmo_idx_CH3CH2O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H5OH) * nall(icell,patmo_idx_C2H5OH)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_HCOOH) * nall(icell,patmo_idx_HCOOH)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H7Oj) * nall(icell,patmo_idx_C2H7Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH3CHOH) * nall(icell,patmo_idx_CH3CHOH)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H2O) * nall(icell,patmo_idx_C2H2O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_HOj) * nall(icell,patmo_idx_HOj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_HO) * nall(icell,patmo_idx_HO)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH2O) * nall(icell,patmo_idx_CH2O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH2OH) * nall(icell,patmo_idx_CH2OH)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H4Oj) * nall(icell,patmo_idx_C2H4Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H2Oj) * nall(icell,patmo_idx_C2H2Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_H2O2) * nall(icell,patmo_idx_H2O2) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH5Oj) * nall(icell,patmo_idx_CH5Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH3Oj) * nall(icell,patmo_idx_CH3Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_H2Oj) * nall(icell,patmo_idx_H2Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_HCCO) * nall(icell,patmo_idx_HCCO)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH3O2j) * nall(icell,patmo_idx_CH3O2j) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H3O) * nall(icell,patmo_idx_C2H3O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CHOj) * nall(icell,patmo_idx_CHOj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_H3Oj) * nall(icell,patmo_idx_H3Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH4O2) * nall(icell,patmo_idx_CH4O2) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_HO2j) * nall(icell,patmo_idx_HO2j) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_O) * nall(icell,patmo_idx_O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH4Oj) * nall(icell,patmo_idx_CH4Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH3O) * nall(icell,patmo_idx_CH3O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH2O2) * nall(icell,patmo_idx_CH2O2) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH2Oj) * nall(icell,patmo_idx_CH2Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CHO2j) * nall(icell,patmo_idx_CHO2j) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2H3O_c) * nall(icell,patmo_idx_C2H3O_c)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CO) * nall(icell,patmo_idx_CO)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH3COCH3) * nall(icell,patmo_idx_CH3COCH3)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CO2) * nall(icell,patmo_idx_CO2) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_H2O) * nall(icell,patmo_idx_H2O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_COj) * nall(icell,patmo_idx_COj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C3H3O) * nall(icell,patmo_idx_C3H3O)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CH2CHO) * nall(icell,patmo_idx_CH2CHO)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C3H7Oj) * nall(icell,patmo_idx_C3H7Oj)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_HO2) * nall(icell,patmo_idx_HO2) * 2d0
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_CHO) * nall(icell,patmo_idx_CHO)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_COOH) * nall(icell,patmo_idx_COOH)
      getTotalMassNuclei_O = getTotalMassNuclei_O + m(patmo_idx_C2HOj) * nall(icell,patmo_idx_C2HOj)
    end do

  end function

  !***************************
  function getTotalMassNuclei_Kr()
    use patmo_commons
    use patmo_parameters
    implicit none
    integer::icell
    real*8::getTotalMassNuclei_Kr
    real*8::m(speciesNumber)

    m(:) = getSpeciesMass()

    getTotalMassNuclei_Kr = 0d0

    do icell=1,cellsNumber
      getTotalMassNuclei_Kr = getTotalMassNuclei_Kr + m(patmo_idx_Kr) * nall(icell,patmo_idx_Kr)
    end do

  end function

  !***************************
  function getTotalMassNuclei_Ar()
    use patmo_commons
    use patmo_parameters
    implicit none
    integer::icell
    real*8::getTotalMassNuclei_Ar
    real*8::m(speciesNumber)

    m(:) = getSpeciesMass()

    getTotalMassNuclei_Ar = 0d0

    do icell=1,cellsNumber
      getTotalMassNuclei_Ar = getTotalMassNuclei_Ar + m(patmo_idx_Ar) * nall(icell,patmo_idx_Ar)
    end do

  end function

  !***************************
  function getTotalMassNuclei_He()
    use patmo_commons
    use patmo_parameters
    implicit none
    integer::icell
    real*8::getTotalMassNuclei_He
    real*8::m(speciesNumber)

    m(:) = getSpeciesMass()

    getTotalMassNuclei_He = 0d0

    do icell=1,cellsNumber
      getTotalMassNuclei_He = getTotalMassNuclei_He + m(patmo_idx_He) * nall(icell,patmo_idx_He)
    end do

  end function

  !*******************
  !returns an array with the list of the reactions verbatims
  function getReactionsVerbatim()
    use patmo_commons
    use patmo_parameters
    implicit none
    character(len=maxNameLength)::getReactionsVerbatim(reactionsNumber)

    getReactionsVerbatim(:) = reactionsVerbatim(:)

  end function getReactionsVerbatim

  !*****************
  !load reaction verbatim from file
  subroutine loadReactionsVerbatim()
    use patmo_commons
    use patmo_parameters
    implicit none
    integer::ios,i, unit
    character(len=maxNameLength)::cout

    open(newunit=unit, file="reactionsVerbatim.dat",iostat=ios,status="old")
    if(ios/=0) then
      print *,"ERROR: problem load in verbatim reaction names"
      stop
    end if

    !loop on file to read verbatims
    do i=1,reactionsNumber
      read(unit, '(a)', iostat=ios) cout
      reactionsVerbatim(i) = trim(cout)
    end do
    close(unit)

  end subroutine loadReactionsVerbatim

  !*****************
  !given a species name returns the index
  function getSpeciesIndex(name,error)
    use patmo_commons
    implicit none
    integer::getSpeciesIndex,i
    logical,optional,intent(in)::error
    logical::riseError
    character(len=*),intent(in)::name
    character(len=maxNameLength)::names(speciesNumber)

    if(present(error)) then
      riseError = error
    else
      riseError = .true.
    end if

    names(:) = getSpeciesNames()

    !loop on species names to find index
    do i=1,speciesNumber
      if(trim(name)==trim(names(i))) then
        getSpeciesIndex = i
        return
      end if
    end do

    if(riseError) then
      print *,"ERROR: index not found for species: ",trim(name)
      print *," Available species are:"
      do i=1,speciesNumber
        print *," "//trim(names(i))
      end do
      stop
    else
      getSpeciesIndex = -1
    end if

  end function getSpeciesIndex

  !****************
  !mean molecular weight in g
  function getMeanMass(n)
    use patmo_commons
    implicit none
    real*8::getMeanMass,m(speciesNumber)
    real*8,intent(in)::n(speciesNumber)

    m(:) = getSpeciesMass()
    getMeanMass = sum(n(1:chemSpeciesNumber) &
        * m(1:chemSpeciesNumber)) &
        / sum(n(1:chemSpeciesNumber))

  end function getMeanMass

end module patmo_utils
