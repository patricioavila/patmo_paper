module patmo_reverseRates
contains

  !compute reverse rates using thermochemistry polynomials
  subroutine computeReverseRates(inTgas)
    use patmo_commons
    use patmo_parameters
    use patmo_thermochemistry
    implicit none
    real*8,intent(in)::inTgas(:)
    integer::i

    ! compute reverse rate using linear fit
  end subroutine computeReverseRates

end module patmo_reverseRates
