module patmo_heating

contains

  ! ********************
  ! radiiogenic heating, erg/s
  ! mass is the planet mass
  function patmo_radiogenicHeating(mass)
    use patmo_constants
    implicit none
    real*8,intent(in)::mass
    real*8::patmo_radiogenicHeating

    !radiogenic heating, erg/s
    patmo_radiogenicHeating = 3d20 * mass / m_earth

  end function patmo_radiogenicHeating

  ! *******************
  !This function calculates the tidal heating following eq. 1 of Henning et al. 2009
  !See also Peale, Cassen & Reynolds 1980
  !Input parameters:
  !rho2: density of body 2, R2: radius of body 2, ecc: eccentricity
  !Q: Tidal Dissipation efficiency, a2: semi-axis body 2
  !gam: body 2 elastic rigidity
  function patmo_tidalHeating(rho2, R2, ecc, Q, gam, mass1, mass2, a2)
    use patmo_constants
    implicit none
    real*8,intent(in)::rho2,R2,ecc,Q,gam,mass1,mass2,a2
    real*8::patmo_tidalHeating,gsup,k2,n,beta
    !superficial gravity
    gsup = G * mass2 / R2**2
    !beta
    beta = gam / (rho2 * gsup * R2)
    !second-order Love Number
    k2 = 1.5d0 * (1d0 / (1d0 + 9.5d0 * beta))
    !mean motion
    n = sqrt(G * mass1 / a2**3)
    !units in CGS, tidal heating in erg s-1
    patmo_tidalheating = (2.1d1 / 2d0) * (k2 * G * mass1**2 * R2**5 * n * ecc**2) / (Q * a2**6)
  end function patmo_tidalHeating

  ! ********************
  ! This subroutine computes the effective temperature
  ! taking into account the tidal heating and the radigenic heating
  ! by following eq. 4 of Henning et al. 2009
  subroutine patmo_computeTeff(rho2, R2, ecc, Q, gam, mass1, mass2, a2)
    use patmo_constants
    use patmo_parameters
    implicit none
    real*8,intent(in)::rho2,R2,ecc,Q,gam,mass1,mass2,a2
    real*8::Ht,H1,H2,T4

    !tidal heating in erg/s
    H1 = patmo_tidalHeating(rho2, R2, ecc, Q, gam, mass1, mass2, a2)

    !radiogenic heating in erg/s
    H2 = patmo_radiogenicHeating(mass2)

    !total heating
    Ht = H1 + H2

    T4 = Ht / (4d0 * pi * R2**2 * sb * 0.9)
    Teff = T4**0.25

  end subroutine patmo_computeTeff

end module patmo_heating
