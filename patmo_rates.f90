module patmo_rates
contains
  subroutine computeRates(inTgas)
    use patmo_commons
    use patmo_parameters
    use patmo_my_func
    implicit none
    real*8,intent(in)::inTgas(cellsNumber)
    real*8::Tgas,T,invT,ntot(cellsNumber),kmax,M
    integer::icell,i

    !total density per layer
    ntot(:) = sum(nAll(:,1:chemSpeciesNumber),2)

    !loop on cells
    do icell=1,cellsNumber
      Tgas = inTgas(icell)
      T = Tgas
      invT = 1d0/Tgas
      M = ntot(icell)
      !H + H -> H2
      krate(icell,1) = toe1(9.13d-33,-6.00d-1,0.00d+0,1.00d-11,0.00d+0,0.00d+0,T,M)

      !H + O -> HO
      krate(icell,2) = toe1(4.33d-32,-1.00d+0,0.00d+0,1.05d-12,-2.00d+0,0.00d+0,T,M)

      !O2 + O -> O3
      krate(icell,3) = toe1(6.00d-34,-2.40d+0,0.00d+0,2.81d-12,0.00d+0,0.00d+0,T,M)

      !C + H2 -> CH2
      krate(icell,4) = toe1(7.00d-32,0.00d+0,0.00d+0,1.70d-11,0.00d+0,5.70d+1,T,M)

      !H + CO -> CHO
      krate(icell,5) = toe1(1.40d-34,0.00d+0,1.00d+2,1.96d-13,0.00d+0,1.37d+3,T,M)

      !HO + H -> H2O
      krate(icell,6) = toe1(6.78d-31,-2.00d+0,0.00d+0,2.09d-10,0.00d+0,0.00d+0,T,M)

      !CH + H2 -> CH3
      krate(icell,7) = toe1(9.00d-31,0.00d+0,5.50d+2,2.01d-10,1.50d-1,0.00d+0,T,M)

      !H + CH2 -> CH3
      krate(icell,8) = toe1(5.63d-31,0.00d+0,0.00d+0,2.01d-11,1.50d-1,0.00d+0,T,M)

      !H + CH3 -> CH4
      krate(icell,9) = toe1(8.90d-29,-1.80d+0,3.18d+2,3.20d-10,1.30d-1,2.50d+1,T,M)

      !H + C2H -> C2H2
      krate(icell,10) = toe1(2.63d-26,-3.10d+0,7.40d+2,3.00d-10,0.00d+0,0.00d+0,T,M)

      !H + C2H2 -> C2H3
      krate(icell,11) = toe1(4.87d-30,-1.07d+0,8.30d+1,1.06d-11,1.64d+0,1.06d+3,T,M)

      !H + C2H3 -> C2H4
      krate(icell,12) = toe1(1.49d-29,-1.00d+0,0.00d+0,2.30d-10,2.00d-1,0.00d+0,T,M)

      !H + C4H -> C4H2
      krate(icell,13) = toe1(2.64d-26,-3.10d+0,7.20d+1,3.00d-10,0.00d+0,0.00d+0,T,M)

      !C2H + C2H -> C4H2
      krate(icell,14) = toe1(5.56d-28,-3.00d+0,3.00d+2,2.60d-10,0.00d+0,0.00d+0,T,M)

      !H + C4H2 -> C4H3
      krate(icell,15) = toe2(5.53d-25,-2.93d+0,1.76d+2,2.66d-12,5.55d+0,0.00d+0,5.64d-13,2.75d+0,5.00d+1,T,M)

      !H + C4H3 -> C4H4
      krate(icell,16) = toe2(5.41d-23,-3.97d+0,1.77d+2,1.39d-10,0.00d+0,0.00d+0,6.22d-12,-3.01d+0,1.62d+2,T,M)

      !H + C2H4 -> C2H5
      krate(icell,17) = toe1(9.23d-29,-1.51d+0,7.30d+1,1.08d-12,5.31d+0,0.00d+0,T,M)

      !CH3 + CH3 -> C2H6
      krate(icell,18) = toe2(3.05d-26,-3.77d+0,6.20d+1,6.78d-11,-3.60d-1,3.00d+1,2.96d-14,-3.23d+0,7.50d+1,T,M)

      !H + C2H5 -> C2H6
      krate(icell,19) = toe1(2.00d-28,-1.50d+0,0.00d+0,1.70d-10,0.00d+0,0.00d+0,T,M)

      !C2H2 + HO -> C2H3O
      krate(icell,20) = toe1(5.00d-30,-1.50d+0,0.00d+0,9.00d-13,2.00d+0,0.00d+0,T,M)

      !HO + CH3 -> CH3OH
      krate(icell,21) = toe1(6.40d-29,0.00d+0,1.03d+3,6.33d-11,1.00d-1,0.00d+0,T,M)

      !C+ + H2 -> CH2+
      krate(icell,22) = toe1(2.10d-29,0d0,0d0,5.07d-10,-1.00d0,0d0,T,M)

      !C2H3+ + H2 -> C2H5+
      krate(icell,23) = toe1(1.49d-29,0d0,0d0,3.60d-10,-1.00d0,0d0,T,M)

      !H+ + H2 -> H3+
      krate(icell,24) = toe1(3.05d-29,0d0,0d0,7.37d-10,-1.00d0,0d0,T,M)

      !CH + H -> C + H2
      krate(icell,25) = kooij(1.30d-10,0d0,8.00d+1,T)

      !C + O2 -> CO + O
      krate(icell,26) = kooij(1.99d-10,0d0,2.01d+3,T)

      !H + HO -> H2 + O
      krate(icell,27) = kooij(6.86d-14,2.80d+0,1.95d+3,T)

      !H + O2 -> HO + O
      krate(icell,28) = kooij(6.73d-10,-5.90d-1,8.15d+3,T)

      !H + C2O -> CO + CH
      krate(icell,29) = kooij(2.19d-11,0d0,0d0,T)

      !H + CH2 -> H2 + CH
      krate(icell,30) = kooij(2.60d-10,0d0,0d0,T)

      !H + CHO -> CO + H2
      krate(icell,31) = kooij(1.50d-10,0d0,0d0,T)

      !H + C2H -> H2 + C2
      krate(icell,32) = kooij(5.99d-11,0.00d+0,1.42d+4,T)

      !H + CO2 -> CO + HO
      krate(icell,33) = kooij(2.51d-10,0.00d+0,1.34d+4,T)

      !H + HO2 -> HO + HO
      krate(icell,34) = kooij(5.50d-11,8.80d-1,0d0,T)

      !H + HO2 -> H2 + O2
      krate(icell,35) = kooij(5.60d-12,1.72d0,0d0,T)

      !H + O3 -> HO + O2
      krate(icell,36) = kooij(2.72d-11,7.50d-1,0d0,T)

      !H + CH2O -> H2 + CHO
      krate(icell,37) = kooij(2.14d-12,1.62d0,1.09d+3,T)

      !H + H2O -> H2 + HO
      krate(icell,38) = kooij(6.82d-12,1.60d+0,9.72d+3,T)

      !H + CH3 -> H2 + CH2
      krate(icell,39) = kooij(1.00d-10,0.00d+0,7.60d+3,T)

      !H + CH4 -> H2 + CH3
      krate(icell,40) = kooij(5.94d-13,3.00d+0,4.05d+3,T)

      !H + H2O2 -> HO + H2O
      krate(icell,41) = kooij(4.00d-11,0d0,1.80d+3,T)

      !H + C2H3 -> C2H2 + H2
      krate(icell,42) = kooij(2.03d-10,-1.00d0,0d0,T)

      !H + C2H2O -> CO + CH3
      krate(icell,43) = kooij(4.99d-12,1.45d0,1.40d+3,T)

      !H + CH3O -> CH2O + H2
      krate(icell,44) = kooij(3.14d-10,-5.80d-1,8.55d+2,T)

      !H + CH2OH -> CH3 + HO
      krate(icell,45) = kooij(1.60d-10,0d0,0d0,T)

      !H + CH2OH -> CH2O + H2
      krate(icell,46) = kooij(1.00d-11,0d0,0d0,T)

      !H + C2H3O -> CH3 + CHO
      krate(icell,47) = kooij(3.32d-11,0d0,0d0,T)

      !H + CH3OH -> CH2OH + H2
      krate(icell,48) = kooij(2.42d-12,2.00d0,2.27d+3,T)

      !H + CH3OH -> CH3O + H2
      krate(icell,49) = kooij(6.64d-11,0d0,3.07d+3,T)

      !H + C2H5 -> CH3 + CH3
      krate(icell,50) = kooij(2.00d-10,0d0,0d0,T)

      !H + C2H5 -> C2H4 + H2
      krate(icell,51) = kooij(3.01d-12,0d0,0d0,T)

      !H + C2H4O -> C2H3O + H2
      krate(icell,52) = kooij(5.27d-13,2.58d0,6.14d+2,T)

      !H + C2H4O -> CH3 + CO + H2
      krate(icell,53) = kooij(4.88d-13,2.75d0,4.86d+2,T)

      !H + C2H4O -> CH4 + CHO
      krate(icell,54) = kooij(8.80d-14,0d0,0d0,T)

      !H + C2H5OH -> C2H5 + H2O
      krate(icell,55) = kooij(9.80d-13,0d0,1.74d+3,T)

      !O + C2 -> CO + C
      krate(icell,56) = kooij(5.99d-10,0d0,0d0,T)

      !O + CH -> HO + C
      krate(icell,57) = kooij(2.52d-11,0d0,2.38d+3,T)

      !O + CH -> CO + H
      krate(icell,58) = kooij(1.02d-10,0d0,9.14d+2,T)

      !O + C2H -> CO + CH
      krate(icell,59) = kooij(1.69d-11,0d0,0d0,T)

      !O + C2O -> CO + CO
      krate(icell,60) = kooij(8.60d-11,0d0,0d0,T)

      !O + CH2 -> CO + H + H
      krate(icell,61) = kooij(1.33d-10,0d0,0d0,T)

      !O + CH2 -> CO + H2
      krate(icell,62) = kooij(6.64d-11,0d0,0d0,T)

      !O + CO2 -> CO + O2
      krate(icell,63) = kooij(1.41d-11,0.00d+0,2.21d+4,T)

      !O + CHO -> CO + HO
      krate(icell,64) = kooij(5.00d-11,0d0,0d0,T)

      !O + CHO -> CO2 + H
      krate(icell,65) = kooij(5.00d-11,0d0,0d0,T)

      !O + HO2 -> HO + O2
      krate(icell,66) = kooij(1.36d-11,7.50d-1,0d0,T)

      !O + O3 -> O2 + O2
      krate(icell,67) = kooij(2.23d-12,7.50d-1,1.58d+3,T)

      !O + C2H2 -> CH2 + CO
      krate(icell,68) = kooij(3.49d-12,1.50d0,8.50d+2,T)

      !O + CH3 -> CH2O + H
      krate(icell,69) = kooij(1.40d-10,0d0,0d0,T)

      !O + CH3 -> CO + H2 + H
      krate(icell,70) = kooij(5.72d-11,0d0,0d0,T)

      !O + CH2O -> CHO + HO
      krate(icell,71) = kooij(1.78d-11,5.70d-1,1.39d+3,T)

      !O + CH2O -> CO + HO + H
      krate(icell,72) = kooij(1.00d-10,0d0,0d0,T)

      !O + H2O2 -> HO2 + HO
      krate(icell,73) = kooij(1.42d-12,2.00d0,2.00d+3,T)

      !O + C2H3 -> C2H2O + H
      krate(icell,74) = kooij(1.60d-10,0d0,0d0,T)

      !O + C2H3 -> C2H2 + HO
      krate(icell,75) = kooij(5.50d-12,2.00d-1,0d0,T)

      !O + CH3O -> CH2O + HO
      krate(icell,76) = kooij(1.00d-11,0d0,0d0,T)

      !O + CH3O -> CH3 + O2
      krate(icell,77) = kooij(3.55d-11,0d0,0d0,T)

      !O + CH2OH -> CH2O + HO
      krate(icell,78) = kooij(7.01d-11,0d0,0d0,T)

      !O + C2H4 -> C2H3O + H
      krate(icell,79) = kooij(6.24d-13,0d0,0d0,T)

      !O + C2H4 -> CH3 + CHO
      krate(icell,80) = kooij(1.50d-12,1.55d0,0d0,T)

      !O + C2H4 -> C2H2O + H2
      krate(icell,81) = kooij(3.82d-14,0d0,0d0,T)

      !O + C2H3O -> C2H2O + HO
      krate(icell,82) = kooij(6.40d-11,0d0,0d0,T)

      !O + C2H3O -> CO2 + CH3
      krate(icell,83) = kooij(1.60d-11,0d0,0d0,T)

      !O + CH3OH -> CH2OH + HO
      krate(icell,84) = kooij(5.71d-11,0d0,2.75d+3,T)

      !O + CH3OH -> CH3O + HO
      krate(icell,85) = kooij(1.66d-11,0d0,2.36d+3,T)

      !O + C2H5 -> C2H4O + H
      krate(icell,86) = kooij(1.33d-10,0d0,0d0,T)

      !O + C2H5 -> C2H4 + HO
      krate(icell,87) = kooij(6.31d-12,3.00d-2,0d0,T)

      !O + C2H5 -> CH2O + CH3
      krate(icell,88) = kooij(2.67d-11,0d0,0d0,T)

      !O + C2H4O -> CH2CHO + HO
      krate(icell,89) = kooij(2.49d-11,0d0,2.52d+3,T)

      !O + C2H4O -> C2H3O + HO
      krate(icell,90) = kooij(2.81d-11,0d0,1.68d+3,T)

      !O + C2H5OH -> CH3CHOH + HO
      krate(icell,91) = kooij(1.03d-13,0d0,0d0,T)

      !C2 + HO -> CO + CH
      krate(icell,92) = kooij(8.30d-12,0d0,0d0,T)

      !C2 + O2 -> CO + CO
      krate(icell,93) = kooij(1.10d-11,0d0,3.81d+2,T)

      !CH + O2 -> CHO + O
      krate(icell,94) = kooij(1.66d-11,0d0,0d0,T)

      !CH + O2 -> CO + HO
      krate(icell,95) = kooij(8.30d-11,0d0,0d0,T)

      !CH + H2O -> CH2O + H
      krate(icell,96) = kooij(2.82d-11,-1.22d0,0d0,T)

      !CH + CH4 -> C2H4 + H
      krate(icell,97) = kooij(9.25d-11,-9.00d-1,0d0,T)

      !CH + C2H4 -> C3H4 + H
      krate(icell,98) = kooij(2.84d-10,-3.10d-1,0d0,T)

      !HO + C2H -> CH2 + CO
      krate(icell,99) = kooij(3.01d-11,0d0,0d0,T)

      !HO + C2H -> C2H2 + O
      krate(icell,100) = kooij(3.01d-11,0d0,0d0,T)

      !HO + CH2 -> CH2O + H
      krate(icell,101) = kooij(3.01d-11,0d0,0d0,T)

      !HO + CHO -> H2O + CO
      krate(icell,102) = kooij(1.69d-10,0d0,0d0,T)

      !HO + HO2 -> H2O + O2
      krate(icell,103) = kooij(7.11d-11,-2.10d-1,0d0,T)

      !HO + O3 -> HO2 + O2
      krate(icell,104) = kooij(3.76d-13,1.99d0,6.03d+2,T)

      !HO + C2H2 -> HCCO + H2
      krate(icell,105) = kooij(1.91d-13,0d0,0d0,T)

      !HO + CH3 -> CH2 + H2O
      krate(icell,106) = kooij(1.20d-10,0d0,1.40d+3,T)

      !HO + CH3 -> CH2O + H2
      krate(icell,107) = kooij(9.10d-11,0d0,1.50d+3,T)

      !HO + H2O2 -> HO2 + H2O
      krate(icell,108) = kooij(1.30d-11,0d0,6.70d+2,T)

      !HO + C2H3 -> C2H2 + H2O
      krate(icell,109) = kooij(5.00d-11,0d0,0d0,T)

      !HO + C2H2O -> CH2OH + CO
      krate(icell,110) = kooij(1.01d-11,0d0,0d0,T)

      !HO + C2H2O -> CH3 + CO2
      krate(icell,111) = kooij(4.98d-12,0d0,0d0,T)

      !HO + C2H2O -> CH2O + CHO
      krate(icell,112) = kooij(4.65d-11,0d0,0d0,T)

      !HO + CH4 -> CH3 + H2O
      krate(icell,113) = kooij(2.96d-13,2.38d0,1.14d+3,T)

      !HO + CH3O -> CH2O + H2O
      krate(icell,114) = kooij(3.01d-11,0d0,0d0,T)

      !HO + CH2OH -> CH2O + H2O
      krate(icell,115) = kooij(4.00d-11,0d0,0d0,T)

      !HO + CH2O2 -> COOH + H2O
      krate(icell,116) = kooij(9.85d-13,0d0,1.04d+3,T)

      !HO + C2H3O -> C2H2O + H2O
      krate(icell,117) = kooij(2.01d-11,0d0,0d0,T)

      !HO + CH3OH -> CH2OH + H2O
      krate(icell,118) = kooij(2.13d-13,2.00d0,0d0,T)

      !HO + CH3OH -> CH3O + H2O
      krate(icell,119) = kooij(1.66d-11,0d0,8.34d+2,T)

      !HO + CH3OH -> CH2O + H2O + H
      krate(icell,120) = kooij(1.10d-12,1.44d0,0d0,T)

      !HO + C2H5 -> C2H4 + H2O
      krate(icell,121) = kooij(4.00d-11,0d0,0d0,T)

      !HO + C2H4O -> CH2CHO + H2O
      krate(icell,122) = kooij(2.66d-11,0d0,1.01d+3,T)

      !HO + C2H4O -> C2H3O + H2O
      krate(icell,123) = kooij(1.66d-11,0d0,1.01d+3,T)

      !HO + C2H4O -> C2H3O_c + H2O
      krate(icell,124) = kooij(2.96d-11,0d0,1.82d+3,T)

      !HO + CH4O2 -> CH3O2 + H2O
      krate(icell,125) = kooij(1.20d-12,0d0,0d0,T)

      !HO + C2H6 -> C2H5 + H2O
      krate(icell,126) = kooij(1.88d-12,1.80d0,5.70d+2,T)

      !HO + C2H5OH -> CH3CH2O + H2O
      krate(icell,127) = kooij(1.60d-13,0d0,0d0,T)

      !O2 + C2O -> CO2 + CO
      krate(icell,128) = kooij(4.05d-13,0d0,0d0,T)

      !O2 + C2O -> CO + CO + O
      krate(icell,129) = kooij(4.21d-13,0d0,0d0,T)

      !O2 + CH2 -> COOH + H
      krate(icell,130) = kooij(5.74d-12,0d0,7.51d+2,T)

      !O2 + CH2 -> H2O + CO
      krate(icell,131) = kooij(4.00d-13,0d0,0d0,T)

      !O2 + CH2 -> CO2 + H + H
      krate(icell,132) = kooij(3.74d-11,-3.30d0,1.44d+3,T)

      !O2 + CH2 -> CO2 + H2
      krate(icell,133) = kooij(2.99d-11,-3.30d0,1.44d+3,T)

      !O2 + CH2 -> CH2O + O
      krate(icell,134) = kooij(3.74d-11,-3.30d0,1.44d+3,T)

      !O2 + CHO -> HO2 + CO
      krate(icell,135) = kooij(6.14d-11,0d0,1.56d+3,T)

      !O2 + CHO -> CO2 + HO
      krate(icell,136) = kooij(1.17d-11,0d0,1.56d+3,T)

      !O2 + CH3 -> CHO + H2O
      krate(icell,137) = kooij(1.66d-12,0d0,0d0,T)

      !O2 + C2H3 -> CH2CHO + O
      krate(icell,138) = kooij(1.79d-11,-6.10d-1,2.65d+3,T)

      !O2 + C2H3 -> C2H2 + HO2
      krate(icell,139) = kooij(6.58d-12,-1.26d0,1.67d+3,T)

      !O2 + C2H3 -> CH2O + CHO
      krate(icell,140) = kooij(2.03d-8,-5.31d0,3.27d+3,T)

      !O2 + CH3O -> CH2O + HO2
      krate(icell,141) = kooij(3.60d-14,0d0,8.80d+2,T)

      !O2 + CH2OH -> CH2O + HO2
      krate(icell,142) = kooij(2.01d-12,0d0,0d0,T)

      !O2 + CH2CHO -> CH3O + CO2
      krate(icell,143) = kooij(1.10d-12,0d0,0d0,T)

      !O2 + C2H5 -> C2H4O + HO
      krate(icell,144) = kooij(4.32d-15,0d0,0d0,T)

      !O2 + C2H5 -> C2H4 + HO2
      krate(icell,145) = kooij(1.61d-12,-1.87d0,7.07d+2,T)

      !O3 + CH3 -> CH3O + O2
      krate(icell,146) = kooij(5.10d-12,0d0,0d0,T)

      !O3 + C2H5 -> CH3CH2O + O2
      krate(icell,147) = kooij(3.32d-14,0d0,0d0,T)

      !C2H + C2H -> C2H2 + C2
      krate(icell,148) = kooij(3.01d-12,0d0,0d0,T)

      !CH2 + CH2 -> C2H2 + H + H
      krate(icell,149) = kooij(2.99d-10,0d0,3.97d+2,T)

      !CH2 + CH2 -> C2H2 + H2
      krate(icell,150) = kooij(1.80d-10,0d0,4.00d+2,T)

      !CHO + CHO -> CO + CO + H2
      krate(icell,151) = kooij(3.64d-11,0d0,0d0,T)

      !CHO + CHO -> CH2O + CO
      krate(icell,152) = kooij(5.00d-11,0d0,0d0,T)

      !HO2 + HO2 -> H2 + O2 + O2
      krate(icell,153) = kooij(1.49d-12,0d0,5.03d+2,T)

      !C2H + CH2 -> C2H2 + CH
      krate(icell,154) = kooij(3.01d-11,0d0,0d0,T)

      !C2H + HO2 -> HCCO + HO
      krate(icell,155) = kooij(3.01d-11,0d0,0d0,T)

      !C2H + CH3 -> C3H3 + H
      krate(icell,156) = kooij(4.00d-11,0d0,0d0,T)

      !C2H + CH2O -> C2H2 + CO + H
      krate(icell,157) = kooij(4.00d-11,0d0,0d0,T)

      !C2H + C2H3 -> C2H2 + C2H2
      krate(icell,158) = kooij(1.60d-12,0d0,0d0,T)

      !C2H + CH4 -> C2H2 + CH3
      krate(icell,159) = kooij(1.20d-11,0d0,4.91d+2,T)

      !C2H + CH3O -> CH2O + C2H2
      krate(icell,160) = kooij(4.00d-11,0d0,0d0,T)

      !C2H + CH2OH -> CH2O + C2H2
      krate(icell,161) = kooij(5.99d-11,0d0,0d0,T)

      !C2H + CH2OH -> C3H3 + HO
      krate(icell,162) = kooij(2.01d-11,0d0,0d0,T)

      !C2H + C2H4 -> C4H4 + H
      krate(icell,163) = kooij(1.22d-10,0d0,0d0,T)

      !C2H + CH3OH -> C2H2 + CH2OH
      krate(icell,164) = kooij(1.00d-11,0d0,0d0,T)

      !C2H + CH3OH -> C2H2 + CH3O
      krate(icell,165) = kooij(2.01d-12,0d0,0d0,T)

      !C2H + C2H5 -> C2H4 + C2H2
      krate(icell,166) = kooij(3.01d-12,0d0,0d0,T)

      !C2H + C2H5 -> C3H3 + CH3
      krate(icell,167) = kooij(3.01d-11,0d0,0d0,T)

      !C2H + C2H6 -> C2H5 + C2H2
      krate(icell,168) = kooij(4.72d-11,5.40d-1,0d0,T)

      !C2O + C2H4 -> C3H4 + CO
      krate(icell,169) = kooij(1.00d-14,0d0,0d0,T)

      !C2O + C2H4O -> C2H4 + CO + CO
      krate(icell,170) = kooij(1.60d-12,0d0,0d0,T)

      !C2O + C2H4O -> CH2O + C2H2 + CO
      krate(icell,171) = kooij(1.00d-14,0d0,0d0,T)

      !CH2 + CHO -> CH3 + CO
      krate(icell,172) = kooij(3.01d-11,0d0,0d0,T)

      !CH2 + CO2 -> CH2O + CO
      krate(icell,173) = kooij(3.90d-14,0d0,0d0,T)

      !CH2 + CH3 -> C2H4 + H
      krate(icell,174) = kooij(1.20d-10,0d0,0d0,T)

      !CH2 + CH2O -> CH3 + CHO
      krate(icell,175) = kooij(1.00d-14,0d0,0d0,T)

      !CH2 + H2O2 -> CH3 + HO2
      krate(icell,176) = kooij(1.00d-14,0d0,0d0,T)

      !CH2 + C2H3 -> C2H2 + CH3
      krate(icell,177) = kooij(8.00d-11,0d0,0d0,T)

      !CH2 + C2H2O -> C2H4 + CO
      krate(icell,178) = kooij(2.00d-10,-2.40d0,0d0,T)

      !CH2 + CH3O -> CH2O + CH3
      krate(icell,179) = kooij(3.01d-11,0d0,0d0,T)

      !CH2 + CH2OH -> C2H4 + HO
      krate(icell,180) = kooij(4.00d-11,0d0,0d0,T)

      !CH2 + CH2OH -> CH2O + CH3
      krate(icell,181) = kooij(2.01d-12,0d0,0d0,T)

      !CH2 + C2H3O -> C2H2O + CH3
      krate(icell,182) = kooij(3.01d-11,0d0,0d0,T)

      !CH2 + C2H5 -> C2H4 + CH3
      krate(icell,183) = kooij(8.00d-11,0d0,0d0,T)

      !CHO + O3 -> CO2 + O2 + H
      krate(icell,184) = kooij(8.30d-13,0d0,0d0,T)

      !CHO + CH3 -> CH4 + CO
      krate(icell,185) = kooij(2.01d-10,0d0,0d0,T)

      !CHO + CH3O -> CH3OH + CO
      krate(icell,186) = kooij(1.50d-10,0d0,0d0,T)

      !CHO + CH2OH -> CH3OH + CO
      krate(icell,187) = kooij(2.01d-10,0d0,0d0,T)

      !CHO + CH2OH -> CH2O + CH2O
      krate(icell,188) = kooij(3.01d-10,0d0,0d0,T)

      !CHO + C2H3O -> C2H4O + CO
      krate(icell,189) = kooij(1.50d-11,0d0,0d0,T)

      !CHO + C2H5 -> C2H6 + CO
      krate(icell,190) = kooij(2.01d-10,0d0,0d0,T)

      !HO2 + O3 -> HO + O2 + O2
      krate(icell,191) = kooij(1.66d-13,0d0,1.41d+3,T)

      !HO2 + CH3 -> CH3O + HO
      krate(icell,192) = kooij(3.01d-11,0d0,0d0,T)

      !HO2 + H2O2 -> H2O + HO + O2
      krate(icell,193) = kooij(1.00d-13,0d0,0d0,T)

      !HO2 + CH3O -> CH2O + H2O2
      krate(icell,194) = kooij(5.00d-13,0d0,0d0,T)

      !HO2 + CH2OH -> CH2O + H2O2
      krate(icell,195) = kooij(2.01d-11,0d0,0d0,T)

      !HO2 + C2H5 -> CH3CH2O + HO
      krate(icell,196) = kooij(4.98d-11,0d0,0d0,T)

      !HO2 + C2H5 -> C2H4 + H2O2
      krate(icell,197) = kooij(5.00d-13,0d0,0d0,T)

      !CH3 + C2H3 -> C3H5 + H
      krate(icell,198) = kooij(1.20d-10,0d0,0d0,T)

      !CH3 + C2H3 -> CH4 + C2H2
      krate(icell,199) = kooij(3.00d-11,0d0,0d0,T)

      !CH3 + C2H2O -> C2H5 + CO
      krate(icell,200) = kooij(8.30d-12,0d0,0d0,T)

      !CH3 + CH3O -> CH2O + CH4
      krate(icell,201) = kooij(4.00d-11,0d0,0d0,T)

      !CH3 + CH2OH -> CH2O + CH4
      krate(icell,202) = kooij(4.00d-12,0d0,0d0,T)

      !CH3 + C2H3O -> C2H6 + CO
      krate(icell,203) = kooij(5.43d-11,0d0,0d0,T)

      !CH3 + C2H3O -> C2H2O + CH4
      krate(icell,204) = kooij(1.01d-11,0d0,0d0,T)

      !CH3 + C2H5 -> C2H4 + CH4
      krate(icell,205) = kooij(1.50d-12,0d0,0d0,T)

      !HCCO + H -> CH2 + CO
      krate(icell,206) = kooij(2.49d-10,0d0,0d0,T)

      !HCCO + O -> CO + CO + H
      krate(icell,207) = kooij(1.60d-10,0d0,0d0,T)

      !HCCO + O -> CO2 + CH
      krate(icell,208) = kooij(4.90d-11,0d0,5.60d+2,T)

      !HCCO + O2 -> CO2 + CO + H
      krate(icell,209) = kooij(2.44d-13,0d0,4.31d+2,T)

      !HCCO + O2 -> CO + CO + HO
      krate(icell,210) = kooij(2.71d-13,0d0,4.31d+2,T)

      !HCCO + C2H2 -> C3H3 + CO
      krate(icell,211) = kooij(1.66d-14,0d0,0d0,T)

      !COOH + CO -> CO2 + CHO
      krate(icell,212) = kooij(1.00d-14,0d0,0d0,T)

      !COOH + O2 -> CO2 + HO2
      krate(icell,213) = kooij(2.09d-12,0d0,0d0,T)

      !COOH + C2H2 -> C2H3 + CO2
      krate(icell,214) = kooij(3.01d-14,0d0,0d0,T)

      !COOH + C2H4 -> C2H5 + CO2
      krate(icell,215) = kooij(1.00d-14,0d0,0d0,T)

      !C2H3 + C2H3 -> C2H4 + C2H2
      krate(icell,216) = kooij(1.40d-10,0d0,0d0,T)

      !CH3O + CH3O -> CH3OH + CH2O
      krate(icell,217) = kooij(1.00d-10,0d0,0d0,T)

      !CH3O + CH2OH -> CH3OH + CH2O
      krate(icell,218) = kooij(4.00d-11,0d0,0d0,T)

      !C2H5 + C2H3 -> C2H4 + C2H4
      krate(icell,219) = kooij(8.00d-13,0d0,0d0,T)

      !C2H5 + C2H5 -> C2H6 + C2H4
      krate(icell,220) = kooij(2.41d-12,0d0,0d0,T)

      !C2H5 + C2H3 -> C2H2 + C2H6
      krate(icell,221) = kooij(8.00d-13,0d0,0d0,T)

      !CH2O + CH3O -> CH3OH + CHO
      krate(icell,222) = kooij(1.69d-13,0d0,1.50d+3,T)

      !H2O2 + C2H3 -> C2H4 + HO2
      krate(icell,223) = kooij(5.45d-14,0d0,0d0,T)

      !C2H3 + CH3O -> CH2O + C2H4
      krate(icell,224) = kooij(4.00d-11,0d0,0d0,T)

      !C2H3 + CH2OH -> CH2O + C2H4
      krate(icell,225) = kooij(5.00d-11,0d0,0d0,T)

      !C2H3 + CH2OH -> C3H5 + HO
      krate(icell,226) = kooij(2.01d-11,0d0,0d0,T)

      !C2H3 + C2H3O -> C3H3O + CH3
      krate(icell,227) = kooij(3.01d-11,0d0,0d0,T)

      !CH3O + C2H3O -> C2H2O + CH3OH
      krate(icell,228) = kooij(1.00d-11,0d0,0d0,T)

      !CH3O + C2H3O -> CH2O + C2H4O
      krate(icell,229) = kooij(1.00d-11,0d0,0d0,T)

      !CH3O + C2H5 -> CH2O + C2H6
      krate(icell,230) = kooij(4.00d-11,0d0,0d0,T)

      !CH3O + C2H4O -> CH3OH + C2H3O
      krate(icell,231) = kooij(8.30d-15,0d0,0d0,T)

      !CH3O2 + H -> CH3O + HO
      krate(icell,232) = kooij(1.60d-10,0d0,0d0,T)

      !CH3O2 + O -> CH3O + O2
      krate(icell,233) = kooij(8.30d-11,0d0,0d0,T)

      !CH3O2 + HO -> CH3OH + O2
      krate(icell,234) = kooij(1.00d-10,0d0,0d0,T)

      !CH3O2 + O2 -> HCOOH + HO2
      krate(icell,235) = kooij(3.50d-14,0d0,0d0,T)

      !CH3O2 + C2H -> CH3O + HCCO
      krate(icell,236) = kooij(4.00d-11,0d0,0d0,T)

      !CH3O2 + CH2 -> CH3O + CH2O
      krate(icell,237) = kooij(3.01d-11,0d0,0d0,T)

      !CH3O2 + HO2 -> CH4O2 + O2
      krate(icell,238) = kooij(1.57d-11,0d0,0d0,T)

      !CH3O2 + CH3 -> CH3O + CH3O
      krate(icell,239) = kooij(4.00d-11,0d0,0d0,T)

      !CH3O2 + C2H3 -> C2H3O_c + CH3O
      krate(icell,240) = kooij(4.00d-11,0d0,0d0,T)

      !CH3O2 + CH3O -> CH4O2 + CH2O
      krate(icell,241) = kooij(5.00d-13,0d0,0d0,T)

      !CH3O2 + CH2OH -> CH4O2 + CH2O
      krate(icell,242) = kooij(2.01d-11,0d0,0d0,T)

      !CH3O2 + C2H5 -> CH3CH2O + CH3O
      krate(icell,243) = kooij(4.00d-11,0d0,0d0,T)

      !CH2OH + CH2OH -> CH3OH + CH2O
      krate(icell,244) = kooij(8.00d-12,0d0,0d0,T)

      !C2H3O + C2H3O -> C2H2O + C2H4O
      krate(icell,245) = kooij(1.49d-11,0d0,0d0,T)

      !CH2OH + C2H5 -> CH2O + C2H6
      krate(icell,246) = kooij(4.00d-12,0d0,0d0,T)

      !CH2OH + C2H5 -> CH3OH + C2H4
      krate(icell,247) = kooij(4.00d-12,0d0,0d0,T)

      !CH3O2 + CH3O2 -> CH3O + CH3O + O2
      krate(icell,248) = kooij(7.40d-13,0d0,5.19d+2,T)

      !CH3O2 + CH3O2 -> CH3OH + CH2O + O2
      krate(icell,249) = kooij(2.22d-13,0d0,0d0,T)

      !CH2CHO + C2H4O -> CH3COCH3 + CHO
      krate(icell,250) = kooij(2.84d-13,0d0,0d0,T)

      !CH3CHOH + H -> C2H4O + H2
      krate(icell,251) = kooij(3.32d-11,0d0,0d0,T)

      !CH3CHOH + O -> C2H4O + HO
      krate(icell,252) = kooij(3.16d-10,0d0,0d0,T)

      !CH3CHOH + O2 -> C2H4O + HO2
      krate(icell,253) = kooij(1.90d-11,0d0,2.80d+3,T)

      !CH3CH2O + O2 -> C2H4O + HO2
      krate(icell,254) = kooij(7.11d-14,0d0,5.52d+2,T)

      !CH3CH2O + C2H5 -> C2H6 + C2H4O
      krate(icell,255) = kooij(6.46d-8,0d0,0d0,T)

      !CH3CH2O + C2H5 -> C2H4 + C2H5OH
      krate(icell,256) = kooij(1.14d-7,0d0,0d0,T)

      !CH3CH2O + CH3CH2O -> C2H5OH + C2H4O
      krate(icell,257) = kooij(6.72d-10,0d0,0d0,T)

      !CH2 + C3H3 -> C4H4 + H
      krate(icell,258) = kooij(3.00d-11,0.00d+0,0.00d+0,T)

      !C2 + CH4 -> C2H + CH3
      krate(icell,259) = kooij(5.05d-11,0.00d+0,2.97d+2,T)

      !C2H + C2H2 -> C4H2 + H
      krate(icell,260) = kooij(1.30d-10,0.00d+0,0.00d+0,T)

      !C4H + H2 -> C4H2 + H
      krate(icell,261) = kooij(2.18d-12,2.17d+0,4.78d+2,T)

      !C4H + CH4 -> C4H2 + CH3
      krate(icell,262) = kooij(1.20d-11,0.00d+0,4.91d+2,T)

      !C4H + C2H6 -> C4H2 + C2H5
      krate(icell,263) = kooij(4.72d-11,5.40d-1,0.00d+0,T)

      !HO2 + C2H4 -> C2H4O + HO
      krate(icell,264) = kooij(3.70d-12,0.00d+0,8.65d+3,T)

      !C+ + CH2O -> CH2+ + CO
      krate(icell,265) = kooij(2.34d-9,0d0,0d0,T)

      !C+ + CH2O -> CHO+ + CH
      krate(icell,266) = kooij(7.80d-10,0d0,0d0,T)

      !C+ + CH2O -> CH2O+ + C
      krate(icell,267) = kooij(7.80d-10,0d0,0d0,T)

      !C+ + HCOOH -> CHO+ + CHO
      krate(icell,268) = kooij(3.30d-9,0d0,0d0,T)

      !C+ + CH4 -> C2H3+ + H
      krate(icell,269) = kooij(1.03d-9,0d0,0d0,T)

      !C+ + CH4 -> C2H2+ + H2
      krate(icell,270) = kooij(4.21d-10,0d0,0d0,T)

      !C+ + CH3OH -> CH4O+ + C
      krate(icell,271) = kooij(1.35d-9,0d0,0d0,T)

      !C+ + CH3OH -> CH3O+ + CH
      krate(icell,272) = kooij(1.23d-9,0d0,0d0,T)

      !C+ + CH3OH -> CH3+ + CHO
      krate(icell,273) = kooij(1.19d-9,0d0,0d0,T)

      !C+ + CH3OH -> CHO+ + CH3
      krate(icell,274) = kooij(3.28d-10,0d0,0d0,T)

      !C+ + CO2 -> CO+ + CO
      krate(icell,275) = kooij(1.60d-9,0d0,0d0,T)

      !C+ + C2H4 -> C3H2+ + H2
      krate(icell,276) = kooij(5.20d-10,0d0,0d0,T)

      !C+ + C2H4 -> C3H3+ + H
      krate(icell,277) = kooij(3.90d-10,0d0,0d0,T)

      !C+ + C2H4 -> C2H4+ + C
      krate(icell,278) = kooij(1.95d-10,0d0,0d0,T)

      !C+ + C2H4 -> C2H3+ + CH
      krate(icell,279) = kooij(1.95d-10,0d0,0d0,T)

      !C+ + C2H6 -> C3H3+ + H2 + H
      krate(icell,280) = kooij(4.80d-10,0d0,0d0,T)

      !C+ + C2H6 -> C2H3+ + CH3
      krate(icell,281) = kooij(4.80d-10,0d0,0d0,T)

      !C+ + C2H6 -> C2H4+ + CH2
      krate(icell,282) = kooij(3.20d-10,0d0,0d0,T)

      !C+ + C2H6 -> C2H5+ + CH
      krate(icell,283) = kooij(3.20d-10,0d0,0d0,T)

      !C+ + H2O -> CHO+ + H
      krate(icell,284) = kooij(2.70d-9,0d0,0d0,T)

      !C+ + O2 -> CO+ + O
      krate(icell,285) = kooij(7.81d-10,0d0,0d0,T)

      !C+ + O2 -> O+ + CO
      krate(icell,286) = kooij(4.39d-10,0d0,0d0,T)

      !CH+ + CH2O -> CHO+ + CH2
      krate(icell,287) = kooij(9.60d-10,0d0,0d0,T)

      !CH+ + CH2O -> CH3+ + CO
      krate(icell,288) = kooij(9.60d-10,0d0,0d0,T)

      !CH+ + CH2O -> CH3O+ + C
      krate(icell,289) = kooij(9.60d-10,0d0,0d0,T)

      !CH+ + CH4 -> C2H3+ + H2
      krate(icell,290) = kooij(1.09d-9,0d0,0d0,T)

      !CH+ + CH4 -> C2H2+ + H2 + H
      krate(icell,291) = kooij(1.43d-10,0d0,0d0,T)

      !CH+ + CH4 -> C2H4+ + H
      krate(icell,292) = kooij(6.50d-11,0d0,0d0,T)

      !CH+ + CH3OH -> CH3+ + CH2O
      krate(icell,293) = kooij(1.45d-9,0d0,0d0,T)

      !CH+ + CH3OH -> CH5O+ + C
      krate(icell,294) = kooij(1.16d-9,0d0,0d0,T)

      !CH+ + CH3OH -> CH3O+ + CH2
      krate(icell,295) = kooij(2.90d-10,0d0,0d0,T)

      !CH+ + CO -> CHO+ + C
      krate(icell,296) = kooij(7.00d-12,0d0,0d0,T)

      !CH+ + CO2 -> CHO+ + CO
      krate(icell,297) = kooij(1.60d-9,0d0,0d0,T)

      !CH+ + C2H2 -> C3H2+ + H
      krate(icell,298) = kooij(2.40d-9,0d0,0d0,T)

      !CH+ + H2 -> CH2+ + H
      krate(icell,299) = kooij(1.20d-9,0d0,0d0,T)

      !CH+ + H2O -> CHO+ + H2
      krate(icell,300) = kooij(1.35d-9,0d0,0d0,T)

      !CH+ + H2O -> CH2O+ + H
      krate(icell,301) = kooij(6.75d-10,0d0,0d0,T)

      !CH+ + H2O -> H3O+ + C
      krate(icell,302) = kooij(6.75d-10,0d0,0d0,T)

      !CHO2+ + CH4 -> CH5+ + CO2
      krate(icell,303) = kooij(1.30d-10,0d0,0d0,T)

      !CHO2+ + C2H2 -> C2H3+ + CO2
      krate(icell,304) = kooij(1.37d-9,0d0,0d0,T)

      !CHO2+ + H2O -> H3O+ + CO2
      krate(icell,305) = kooij(3.00d-9,0d0,0d0,T)

      !CHO+ + CO2 -> CHO2+ + CO
      krate(icell,306) = kooij(1.25d-9,0d0,0d0,T)

      !CH+ + O -> CO+ + H
      krate(icell,307) = kooij(3.50d-10,0d0,0d0,T)

      !CH+ + O -> H+ + CO
      krate(icell,308) = kooij(3.50d-10,0d0,0d0,T)

      !CH+ + O2 -> CHO+ + O
      krate(icell,309) = kooij(4.85d-10,0d0,0d0,T)

      !CH+ + O2 -> CO+ + HO
      krate(icell,310) = kooij(2.43d-10,0d0,0d0,T)

      !CH+ + O2 -> O+ + CHO
      krate(icell,311) = kooij(2.43d-10,0d0,0d0,T)

      !CHO+ + CH2O -> CH3O+ + CO
      krate(icell,312) = kooij(6.10d-10,0d0,0d0,T)

      !CHO+ + HCOOH -> CH3O2+ + CO
      krate(icell,313) = kooij(1.80d-9,0d0,0d0,T)

      !CHO+ + CH3OH -> CH5O+ + CO
      krate(icell,314) = kooij(2.40d-9,0d0,0d0,T)

      !CHO+ + C2H2 -> C2H3+ + CO
      krate(icell,315) = kooij(1.36d-9,0d0,0d0,T)

      !CHO+ + C2H2O -> C2H3O+ + CO
      krate(icell,316) = kooij(1.80d-9,0d0,0d0,T)

      !CHO+ + C2H4O -> C2H5O+ + CO
      krate(icell,317) = kooij(3.69d-9,0d0,0d0,T)

      !CHO+ + C2H6 -> C2H7+ + CO
      krate(icell,318) = kooij(1.20d-10,0d0,0d0,T)

      !CHO+ + C2H5OH -> C2H7O+ + CO
      krate(icell,319) = kooij(2.20d-9,0d0,0d0,T)

      !CHO+ + H2O -> H3O+ + CO
      krate(icell,320) = kooij(2.50d-9,0d0,0d0,T)

      !CHO+ + O2 -> HO2+ + CO
      krate(icell,321) = kooij(4.80d-10,0d0,0d0,T)

      !CH2+ + CH2O -> CH3+ + CHO
      krate(icell,322) = kooij(6.40d-10,0d0,0d0,T)

      !CH2+ + CH2O -> CHO+ + CH3
      krate(icell,323) = kooij(2.80d-9,0d0,0d0,T)

      !CH2+ + CH2O -> C2H3O+ + H
      krate(icell,324) = kooij(3.30d-10,0d0,0d0,T)

      !CH2+ + CH4 -> C2H4+ + H2
      krate(icell,325) = kooij(8.40d-10,0d0,0d0,T)

      !CH2+ + CH4 -> C2H5+ + H
      krate(icell,326) = kooij(3.60d-10,0d0,0d0,T)

      !CH2+ + CH3OH -> CH3O+ + CH3
      krate(icell,327) = kooij(1.30d-9,0d0,0d0,T)

      !CH2+ + CH3OH -> CH5O+ + CH
      krate(icell,328) = kooij(1.30d-9,0d0,0d0,T)

      !CH2+ + CO -> CHO+ + CH
      krate(icell,329) = kooij(4.99d-12,-2.50d0,0d0,T)

      !CH2+ + CO2 -> CH2O+ + CO
      krate(icell,330) = kooij(1.60d-9,0d0,0d0,T)

      !CH2+ + C2H2 -> C3H3+ + H
      krate(icell,331) = kooij(2.50d-9,0d0,0d0,T)

      !CH2+ + H2 -> CH3+ + H
      krate(icell,332) = kooij(1.60d-9,0d0,0d0,T)

      !CH2+ + H2O -> CH3O+ + H
      krate(icell,333) = kooij(2.90d-9,0d0,0d0,T)

      !CH2+ + H2O -> H3O+ + CH
      krate(icell,334) = kooij(2.90d-9,0d0,0d0,T)

      !CH2+ + O2 -> CHO+ + HO
      krate(icell,335) = kooij(9.10d-10,0d0,0d0,T)

      !CH2+ + O2 -> CH2O+ + O
      krate(icell,336) = kooij(9.10d-10,0d0,0d0,T)

      !CH2O+ + CH2O -> CH3O+ + CHO
      krate(icell,337) = kooij(1.65d-9,0d0,0d0,T)

      !CH2O+ + CH2O -> CH4O+ + CO
      krate(icell,338) = kooij(1.24d-10,0d0,0d0,T)

      !CH2O+ + CH4 -> CH3O+ + CH3
      krate(icell,339) = kooij(9.35d-11,0d0,0d0,T)

      !CH2O+ + CH4 -> C2H5O+ + H
      krate(icell,340) = kooij(1.65d-11,0d0,0d0,T)

      !CH2O+ + CH3OH -> CH5O+ + CHO
      krate(icell,341) = kooij(2.16d-9,0d0,0d0,T)

      !CH2O+ + CH3OH -> CH3O+ + CH3O
      krate(icell,342) = kooij(2.40d-10,0d0,0d0,T)

      !CH2O+ + H2O -> H3O+ + CHO
      krate(icell,343) = kooij(2.60d-9,0d0,0d0,T)

      !CH2O+ + O2 -> CHO+ + HO2
      krate(icell,344) = kooij(7.70d-11,0d0,0d0,T)

      !CH2O+ + O2 -> H2O2+ + CO
      krate(icell,345) = kooij(3.30d-11,0d0,0d0,T)

      !CH3+ + CH2O -> CHO+ + CH4
      krate(icell,346) = kooij(1.60d-9,0d0,0d0,T)

      !CH3+ + CH4 -> CH5+ + CH2
      krate(icell,347) = kooij(1.50d-9,0d0,0d0,T)

      !CH3+ + CH4 -> C2H5+ + H2
      krate(icell,348) = kooij(1.00d-9,0d0,0d0,T)

      !CH3+ + CH3OH -> CH3O+ + CH4
      krate(icell,349) = kooij(2.30d-9,0d0,0d0,T)

      !CH3+ + C2H2 -> C3H3+ + H2
      krate(icell,350) = kooij(1.20d-9,0d0,0d0,T)

      !CH3+ + C2H4O -> C2H3O+ + CH4
      krate(icell,351) = kooij(2.10d-10,0d0,0d0,T)

      !CH3+ + C2H4O -> CH3O+ + C2H4
      krate(icell,352) = kooij(5.61d-10,0d0,0d0,T)

      !CH3+ + C2H4O -> C2H3+ + CH3OH
      krate(icell,353) = kooij(3.08d-10,0d0,0d0,T)

      !CH3+ + C2H4O -> C2H4+ + CH3O
      krate(icell,354) = kooij(1.98d-10,0d0,0d0,T)

      !CH3+ + H2 -> CH4+ + H
      krate(icell,355) = kooij(5.00d-13,0d0,0d0,T)

      !CH3+ + O -> CHO+ + H2
      krate(icell,356) = kooij(3.08d-10,0d0,0d0,T)

      !CH3+ + O -> H3+ + CO
      krate(icell,357) = kooij(8.80d-11,0d0,0d0,T)

      !CH3+ + O -> CH2O+ + H
      krate(icell,358) = kooij(4.40d-11,0d0,0d0,T)

      !CH3O+ + CH2O -> C2H3O+ + H2O
      krate(icell,359) = kooij(1.81d-12,0d0,0d0,T)

      !CH3O+ + CH2O -> C2H5O+ + O
      krate(icell,360) = kooij(1.81d-12,0d0,0d0,T)

      !CH3O+ + CH2O -> CH5O+ + CO
      krate(icell,361) = kooij(1.21d-12,0d0,0d0,T)

      !CH3O+ + H2O -> H3O+ + CH2O
      krate(icell,362) = kooij(3.00d-11,0d0,0d0,T)

      !CH4+ + CH2O -> CH3O+ + CH3
      krate(icell,363) = kooij(1.98d-9,0d0,0d0,T)

      !CH4+ + CH2O -> CH2O+ + CH4
      krate(icell,364) = kooij(1.62d-9,0d0,0d0,T)

      !CH4+ + CH4 -> CH5+ + CH3
      krate(icell,365) = kooij(1.20d-9,0d0,0d0,T)

      !CH4+ + CH3OH -> CH4O+ + CH4
      krate(icell,366) = kooij(1.80d-9,0d0,0d0,T)

      !CH4+ + CH3OH -> CH5O+ + CH3
      krate(icell,367) = kooij(1.20d-9,0d0,0d0,T)

      !CH4+ + CO -> CHO+ + CH3
      krate(icell,368) = kooij(1.40d-9,0d0,0d0,T)

      !CH4+ + CO -> C2H3O+ + H
      krate(icell,369) = kooij(6.08d-10,0d0,0d0,T)

      !CH4+ + CO2 -> C2H3O+ + HO
      krate(icell,370) = kooij(9.60d-12,0d0,0d0,T)

      !CH4+ + C2H2 -> C2H3+ + CH3
      krate(icell,371) = kooij(1.25d-9,0d0,0d0,T)

      !CH4+ + C2H2 -> C2H2+ + CH4
      krate(icell,372) = kooij(1.13d-9,0d0,0d0,T)

      !CH4+ + C2H2 -> C3H3+ + H2 + H
      krate(icell,373) = kooij(1.25d-10,0d0,0d0,T)

      !CH4+ + C2H4 -> C2H5+ + CH3
      krate(icell,374) = kooij(2.90d-9,0d0,0d0,T)

      !CH4+ + C2H4 -> C2H4+ + CH4
      krate(icell,375) = kooij(2.90d-9,0d0,0d0,T)

      !CH4+ + H2 -> CH5+ + H
      krate(icell,376) = kooij(3.30d-11,0d0,0d0,T)

      !CH4+ + H2O -> H3O+ + CH3
      krate(icell,377) = kooij(3.33d-9,0d0,0d0,T)

      !CH4+ + H2O -> CH5O+ + H
      krate(icell,378) = kooij(1.75d-10,0d0,0d0,T)

      !CH4+ + O2 -> O2+ + CH4
      krate(icell,379) = kooij(4.40d-10,0d0,0d0,T)

      !CH4O+ + CH3OH -> CH5O+ + CH3O
      krate(icell,380) = kooij(2.50d-9,0d0,0d0,T)

      !CH5+ + CH2O -> CH3O+ + CH4
      krate(icell,381) = kooij(4.50d-9,0d0,0d0,T)

      !CH5+ + HCOOH -> CH3O2+ + CH4
      krate(icell,382) = kooij(2.90d-9,0d0,0d0,T)

      !CH5+ + CO -> CHO+ + CH4
      krate(icell,383) = kooij(9.90d-10,0d0,0d0,T)

      !CH5+ + C2H2 -> C2H3+ + CH4
      krate(icell,384) = kooij(1.56d-9,0d0,0d0,T)

      !CH5+ + C2H4 -> C2H5+ + CH4
      krate(icell,385) = kooij(1.00d-9,0d0,0d0,T)

      !CH5+ + C2H6 -> C2H7+ + CH4
      krate(icell,386) = kooij(1.28d-9,0d0,0d0,T)

      !CH5+ + C2H6 -> C2H5+ + CH4 + H2
      krate(icell,387) = kooij(2.25d-10,0d0,0d0,T)

      !CH5+ + H2O -> H3O+ + CH4
      krate(icell,388) = kooij(3.70d-9,0d0,0d0,T)

      !CH5+ + O -> H3O+ + CH2
      krate(icell,389) = kooij(2.16d-10,0d0,0d0,T)

      !CH5+ + O -> CH3O+ + H2
      krate(icell,390) = kooij(4.40d-12,0d0,0d0,T)

      !CH5O+ + CH2O -> C2H5O+ + H2O
      krate(icell,391) = kooij(2.10d-11,0d0,0d0,T)

      !CH5O+ + CH3OH -> C2H7O+ + H2O
      krate(icell,392) = kooij(1.08d-10,0d0,0d0,T)

      !CO+ + CH2O -> CHO+ + CHO
      krate(icell,393) = kooij(1.65d-9,0d0,0d0,T)

      !CO+ + CH2O -> CH2O+ + CO
      krate(icell,394) = kooij(1.35d-9,0d0,0d0,T)

      !CO+ + HCOOH -> CHO+ + CH + O2
      krate(icell,395) = kooij(4.10d-9,0d0,0d0,T)

      !CO+ + CH4 -> CH4+ + CO
      krate(icell,396) = kooij(7.93d-10,0d0,0d0,T)

      !CO+ + CH4 -> CHO+ + CH3
      krate(icell,397) = kooij(4.55d-10,0d0,0d0,T)

      !CO+ + CH4 -> C2H3O+ + H
      krate(icell,398) = kooij(5.20d-11,0d0,0d0,T)

      !CO+ + CO2 -> CO2+ + CO
      krate(icell,399) = kooij(3.70d-9,0d0,0d0,T)

      !CO+ + H2 -> CHO+ + H
      krate(icell,400) = kooij(1.80d-9,0d0,0d0,T)

      !CO+ + H2O -> H2O+ + CO
      krate(icell,401) = kooij(1.72d-9,0d0,0d0,T)

      !CO+ + H2O -> CHO+ + HO
      krate(icell,402) = kooij(8.84d-10,0d0,0d0,T)

      !CO+ + O -> O+ + CO
      krate(icell,403) = kooij(1.40d-10,0d0,0d0,T)

      !CO+ + O2 -> O2+ + CO
      krate(icell,404) = kooij(1.20d-10,0d0,0d0,T)

      !CO2+ + CH4 -> CH4+ + CO2
      krate(icell,405) = kooij(5.50d-10,0d0,0d0,T)

      !CO2+ + C2H2 -> C2H2+ + CO2
      krate(icell,406) = kooij(7.30d-11,0d0,0d0,T)

      !CO2+ + C2H4 -> C2H2+ + CO2 + H2
      krate(icell,407) = kooij(5.04d-10,0d0,0d0,T)

      !CO2+ + C2H4 -> C2H3+ + CO2 + H
      krate(icell,408) = kooij(2.43d-10,0d0,0d0,T)

      !CO2+ + C2H4 -> C2H4+ + CO2
      krate(icell,409) = kooij(1.53d-10,0d0,0d0,T)

      !CO2+ + H -> CHO+ + O
      krate(icell,410) = kooij(2.41d-10,0d0,0d0,T)

      !CO2+ + H -> H+ + CO2
      krate(icell,411) = kooij(4.93d-11,0d0,0d0,T)

      !CO2+ + H2O -> H2O+ + CO2
      krate(icell,412) = kooij(2.04d-9,0d0,0d0,T)

      !CO2+ + O -> O2+ + CO
      krate(icell,413) = kooij(1.64d-10,0d0,0d0,T)

      !CO2+ + O -> O+ + CO2
      krate(icell,414) = kooij(9.62d-11,0d0,0d0,T)

      !CO2+ + O2 -> O2+ + CO2
      krate(icell,415) = kooij(6.30d-11,-1.06d0,0d0,T)

      !C2+ + CH4 -> C3H2+ + H2
      krate(icell,416) = kooij(5.74d-10,0.00d+0,0.00d+0,T)

      !C2+ + CH4 -> C2H+ + CH3
      krate(icell,417) = kooij(2.38d-10,0.00d+0,0.00d+0,T)

      !C2+ + CH4 -> C3H3+ + H
      krate(icell,418) = kooij(2.10d-10,0.00d+0,0.00d+0,T)

      !C2+ + CH4 -> C2H2+ + CH2
      krate(icell,419) = kooij(1.82d-10,0.00d+0,0.00d+0,T)

      !C2+ + H2 -> C2H+ + H
      krate(icell,420) = kooij(1.40d-9,0.00d+0,0.00d+0,T)

      !C2+ + O -> C+ + CO
      krate(icell,421) = kooij(3.10d-10,0.00d+0,0.00d+0,T)

      !C2+ + O -> CO+ + C
      krate(icell,422) = kooij(3.10d-10,0.00d+0,0.00d+0,T)

      !C2H+ + CH4 -> C2H2+ + CH3
      krate(icell,423) = kooij(3.74d-10,0d0,0d0,T)

      !C2H+ + CH4 -> C3H3+ + H2
      krate(icell,424) = kooij(3.74d-10,0d0,0d0,T)

      !C2H+ + C2H2 -> C2H3+ + C2
      krate(icell,425) = kooij(2.40d-9,0d0,0d0,T)

      !C2H+ + C2H2 -> C4H2+ + H
      krate(icell,426) = kooij(2.40d-9,0d0,0d0,T)

      !C2H+ + H2 -> C2H2+ + H
      krate(icell,427) = kooij(1.70d-9,0d0,0d0,T)

      !C2H+ + O -> C+ + CHO
      krate(icell,428) = kooij(3.30d-10,0d0,0d0,T)

      !C2H+ + O -> CH+ + CO
      krate(icell,429) = kooij(3.30d-10,0d0,0d0,T)

      !C2H+ + O -> CHO+ + C
      krate(icell,430) = kooij(3.30d-10,0d0,0d0,T)

      !C2H+ + O -> CO+ + CH
      krate(icell,431) = kooij(3.30d-10,0d0,0d0,T)

      !C2H2+ + C2H2 -> C4H2+ + H2
      krate(icell,432) = kooij(1.25d-9,0d0,0d0,T)

      !C2H2+ + C2H2 -> C2H3+ + C2H
      krate(icell,433) = kooij(1.25d-9,0d0,0d0,T)

      !C2H2+ + C2H4 -> C2H4+ + C2H2
      krate(icell,434) = kooij(8.96d-10,0d0,0d0,T)

      !C2H2+ + C2H4 -> C3H3+ + CH3
      krate(icell,435) = kooij(2.80d-10,0d0,0d0,T)

      !C2H2+ + C2H4 -> C4H5+ + H
      krate(icell,436) = kooij(2.24d-10,0d0,0d0,T)

      !C2H2+ + H2 -> C2H3+ + H
      krate(icell,437) = kooij(1.00d-11,0d0,0d0,T)

      !C2H2+ + O -> CHO+ + CH
      krate(icell,438) = kooij(8.50d-11,0d0,0d0,T)

      !C2H2+ + O -> C2HO+ + H
      krate(icell,439) = kooij(8.50d-11,0d0,0d0,T)

      !C2H3+ + C2H4 -> C2H5+ + C2H2
      krate(icell,440) = kooij(3.80d-10,0d0,0d0,T)

      !C2H3+ + C2H6 -> C2H5+ + C2H4
      krate(icell,441) = kooij(4.80d-10,0d0,0d0,T)

      !C2H3+ + H2 -> C2H4+ + H
      krate(icell,442) = kooij(9.99d-13,0d0,0d0,T)

      !C2H3O+ + C2H4O -> C2H5O+ + C2H2O
      krate(icell,443) = kooij(7.14d-10,0d0,0d0,T)

      !C2H4+ + C2H2 -> C3H3+ + CH3
      krate(icell,444) = kooij(6.73d-10,0d0,0d0,T)

      !C2H4+ + C2H2 -> C4H5+ + H
      krate(icell,445) = kooij(2.37d-10,0d0,0d0,T)

      !C2H5+ + CH2O -> CH3O+ + C2H4
      krate(icell,446) = kooij(3.10d-9,0d0,0d0,T)

      !C2H5+ + HCOOH -> CH3O2+ + C2H4
      krate(icell,447) = kooij(1.50d-9,0d0,0d0,T)

      !C2H5+ + C2H2 -> C4H5+ + H2
      krate(icell,448) = kooij(1.24d-10,0d0,0d0,T)

      !C2H5+ + C2H2 -> C3H3+ + CH4
      krate(icell,449) = kooij(6.65d-11,0d0,0d0,T)

      !C2H5+ + H2 -> C2H6+ + H
      krate(icell,450) = kooij(7.30d-14,0d0,5.89d+2,T)

      !C2H6+ + C2H6 -> C2H5+ + C2H6 + H
      krate(icell,451) = kooij(1.01d-10,0d0,0d0,T)

      !C2H6+ + H2O -> H3O+ + C2H5
      krate(icell,452) = kooij(1.20d-9,0d0,0d0,T)

      !C2H6+ + O2 -> C2H5+ + HO2
      krate(icell,453) = kooij(1.15d-10,0d0,0d0,T)

      !C2H7O+ + CH2O -> C3H7O+ + H2O
      krate(icell,454) = kooij(1.70d-11,0d0,0d0,T)

      !H+ + CH4 -> CH3+ + H2
      krate(icell,455) = kooij(4.50d-9,0d0,0d0,T)

      !H+ + CH4 -> CH4+ + H
      krate(icell,456) = kooij(4.50d-9,0d0,0d0,T)

      !H+ + CO2 -> CHO+ + O
      krate(icell,457) = kooij(3.00d-9,0d0,0d0,T)

      !H+ + C2H6 -> C2H4+ + H2 + H
      krate(icell,458) = kooij(3.90d-9,0d0,0d0,T)

      !H+ + C2H6 -> C2H3+ + H2 + H2
      krate(icell,459) = kooij(3.90d-9,0d0,0d0,T)

      !H+ + C2H6 -> C2H5+ + H2
      krate(icell,460) = kooij(3.90d-9,0d0,0d0,T)

      !H+ + O -> O+ + H
      krate(icell,461) = kooij(3.75d-10,0d0,0d0,T)

      !HO+ + CH4 -> H3O+ + CH2
      krate(icell,462) = kooij(1.31d-9,0d0,0d0,T)

      !HO+ + CH4 -> CH5+ + O
      krate(icell,463) = kooij(1.95d-10,0d0,0d0,T)

      !HO+ + CO -> CHO+ + O
      krate(icell,464) = kooij(8.20d-10,0d0,0d0,T)

      !HO+ + C2H6 -> C2H4+ + H2O + H
      krate(icell,465) = kooij(1.04d-9,0d0,0d0,T)

      !HO+ + C2H6 -> C2H5+ + H2O
      krate(icell,466) = kooij(3.20d-10,0d0,0d0,T)

      !HO+ + C2H6 -> H3O+ + C2H4
      krate(icell,467) = kooij(1.60d-10,0d0,0d0,T)

      !HO+ + C2H6 -> C2H6+ + HO
      krate(icell,468) = kooij(4.80d-11,0d0,0d0,T)

      !HO+ + C2H6 -> C2H7+ + O
      krate(icell,469) = kooij(3.20d-11,0d0,0d0,T)

      !HO+ + H2 -> H2O+ + H
      krate(icell,470) = kooij(1.50d-9,0d0,0d0,T)

      !HO+ + H2O -> H2O+ + HO
      krate(icell,471) = kooij(2.87d-9,0d0,0d0,T)

      !HO+ + H2O -> H3O+ + O
      krate(icell,472) = kooij(2.87d-9,0d0,0d0,T)

      !HO+ + O2 -> O2+ + HO
      krate(icell,473) = kooij(2.00d-10,0d0,0d0,T)

      !HO2+ + H2 -> H3+ + O2
      krate(icell,474) = kooij(3.00d-10,0d0,0d0,T)

      !H2+ + H -> H+ + H2
      krate(icell,475) = kooij(6.40d-10,0d0,0d0,T)

      !H2+ + H2 -> H3+ + H
      krate(icell,476) = kooij(2.11d-9,0d0,0d0,T)

      !H2+ + O2 -> HO2+ + H
      krate(icell,477) = kooij(7.56d-9,0d0,0d0,T)

      !H2O+ + CH4 -> CH4+ + H2O
      krate(icell,478) = kooij(1.20d-9,0d0,0d0,T)

      !H2O+ + CH4 -> H3O+ + CH3
      krate(icell,479) = kooij(1.20d-9,0d0,0d0,T)

      !H2O+ + CO -> CHO+ + HO
      krate(icell,480) = kooij(5.30d-10,0d0,0d0,T)

      !H2O+ + C2H2 -> C2H2+ + H2O
      krate(icell,481) = kooij(1.90d-9,0d0,0d0,T)

      !H2O+ + C2H4 -> C2H4+ + H2O
      krate(icell,482) = kooij(1.60d-9,0d0,0d0,T)

      !H2O+ + C2H4 -> C2H5+ + HO
      krate(icell,483) = kooij(1.60d-9,0d0,0d0,T)

      !H2O+ + C2H6 -> H3O+ + C2H5
      krate(icell,484) = kooij(1.33d-9,0d0,0d0,T)

      !H2O+ + C2H6 -> C2H4+ + H2O + H2
      krate(icell,485) = kooij(1.92d-10,0d0,0d0,T)

      !H2O+ + C2H6 -> C2H6+ + H2O
      krate(icell,486) = kooij(6.40d-11,0d0,0d0,T)

      !H2O+ + C2H6 -> C2H5+ + H2O + H
      krate(icell,487) = kooij(1.60d-11,0d0,0d0,T)

      !H2O+ + H2 -> H3O+ + H
      krate(icell,488) = kooij(1.40d-9,0d0,0d0,T)

      !H2O+ + H2O -> H3O+ + HO
      krate(icell,489) = kooij(1.80d-9,0d0,0d0,T)

      !H2O+ + O -> O2+ + H2
      krate(icell,490) = kooij(3.99d-11,0d0,0d0,T)

      !H2O+ + O2 -> O2+ + H2O
      krate(icell,491) = kooij(2.00d-10,0d0,0d0,T)

      !H2O2+ + CO -> CHO+ + HO2
      krate(icell,492) = kooij(5.50d-11,0d0,0d0,T)

      !H2O2+ + H2O -> H3O+ + HO2
      krate(icell,493) = kooij(1.70d-9,0d0,0d0,T)

      !H3+ + CH2O -> CH3O+ + H2
      krate(icell,494) = kooij(6.30d-9,0d0,0d0,T)

      !H3+ + HCOOH -> CHO+ + H2O + H2
      krate(icell,495) = kooij(4.27d-9,0d0,0d0,T)

      !H3+ + HCOOH -> H3O+ + CO + H2
      krate(icell,496) = kooij(1.83d-9,0d0,0d0,T)

      !H3+ + CH4 -> CH5+ + H2
      krate(icell,497) = kooij(1.60d-9,0d0,0d0,T)

      !H3+ + CO -> CHO+ + H2
      krate(icell,498) = kooij(1.40d-9,0d0,0d0,T)

      !H3+ + C2H2 -> C2H3+ + H2
      krate(icell,499) = kooij(2.90d-9,0d0,0d0,T)

      !H3+ + C2H4 -> C2H5+ + H2
      krate(icell,500) = kooij(1.90d-9,0d0,0d0,T)

      !H3+ + C2H4 -> C2H3+ + H2 + H2
      krate(icell,501) = kooij(1.21d-9,0d0,0d0,T)

      !H3+ + C2H6 -> C2H7+ + H2
      krate(icell,502) = kooij(2.02d-11,0d0,0d0,T)

      !H3+ + C2H6 -> C2H5+ + H2 + H2
      krate(icell,503) = kooij(2.40d-9,0d0,0d0,T)

      !H3+ + H2O -> H3O+ + H2
      krate(icell,504) = kooij(4.30d-9,0d0,0d0,T)

      !H3+ + O2 -> HO2+ + H2
      krate(icell,505) = kooij(7.00d-10,0d0,0d0,T)

      !H3O+ + CH2O -> CH3O+ + H2O
      krate(icell,506) = kooij(3.00d-9,0d0,0d0,T)

      !H3O+ + HCOOH -> CH3O2+ + H2O
      krate(icell,507) = kooij(2.70d-9,0d0,0d0,T)

      !H3O+ + CH3OH -> CH5O+ + H2O
      krate(icell,508) = kooij(2.20d-9,0d0,0d0,T)

      !H3O+ + C2H2O -> C2H3O+ + H2O
      krate(icell,509) = kooij(2.00d-9,0d0,0d0,T)

      !H3O+ + C2H4 -> C2H5+ + H2O
      krate(icell,510) = kooij(6.30d-11,0d0,0d0,T)

      !H3O+ + C2H4O -> C2H5O+ + H2O
      krate(icell,511) = kooij(3.60d-9,0d0,0d0,T)

      !H3O+ + C2H5OH -> C2H7O+ + H2O
      krate(icell,512) = kooij(2.80d-9,0d0,0d0,T)

      !O+ + CH2O -> CH2O+ + O
      krate(icell,513) = kooij(2.10d-9,0d0,0d0,T)

      !O+ + CH2O -> CHO+ + HO
      krate(icell,514) = kooij(1.40d-9,0d0,0d0,T)

      !O+ + HCOOH -> CHO+ + O2 + H
      krate(icell,515) = kooij(3.50d-9,0d0,0d0,T)

      !O+ + HCOOH -> CHO+ + HO2
      krate(icell,516) = kooij(1.50d-9,0d0,0d0,T)

      !O+ + CH4 -> CH4+ + O
      krate(icell,517) = kooij(8.90d-10,0d0,0d0,T)

      !O+ + CH4 -> CH3+ + HO
      krate(icell,518) = kooij(1.10d-10,0d0,0d0,T)

      !O+ + CH3OH -> CH3O+ + HO
      krate(icell,519) = kooij(1.33d-9,0d0,0d0,T)

      !O+ + CH3OH -> CH4O+ + O
      krate(icell,520) = kooij(4.75d-10,0d0,0d0,T)

      !O+ + CH3OH -> CH2O+ + H2O
      krate(icell,521) = kooij(9.50d-11,0d0,0d0,T)

      !O+ + CO2 -> CO2+ + O
      krate(icell,522) = kooij(9.00d-10,0d0,0d0,T)

      !O+ + CO2 -> O2+ + CO
      krate(icell,523) = kooij(9.00d-10,0d0,0d0,T)

      !O+ + C2H6 -> C2H4+ + H2O
      krate(icell,524) = kooij(1.33d-9,0d0,0d0,T)

      !O+ + C2H6 -> C2H5+ + HO
      krate(icell,525) = kooij(5.70d-10,0d0,0d0,T)

      !O+ + H2 -> HO+ + H
      krate(icell,526) = kooij(1.70d-9,0d0,0d0,T)

      !O+ + H2O -> H2O+ + O
      krate(icell,527) = kooij(3.20d-9,0d0,0d0,T)

      !O2+ + CH2O -> CH2O+ + O2
      krate(icell,528) = kooij(2.07d-9,0d0,0d0,T)

      !O2+ + CH2O -> CHO+ + HO2
      krate(icell,529) = kooij(2.30d-10,0d0,0d0,T)

      !O2+ + CH4 -> CH3O2+ + H
      krate(icell,530) = kooij(4.41d-12,0d0,0d0,T)

      !O2+ + CH4 -> CH2O+ + H2O
      krate(icell,531) = kooij(9.45d-13,0d0,0d0,T)

      !O2+ + CH4 -> H2O+ + CH2O
      krate(icell,532) = kooij(9.45d-13,0d0,0d0,T)

      !O2+ + CH3OH -> CH3O+ + HO2
      krate(icell,533) = kooij(5.00d-10,0d0,0d0,T)

      !O2+ + CH3OH -> CH4O+ + O2
      krate(icell,534) = kooij(5.00d-10,0d0,0d0,T)

      !O2+ + C2H4 -> C2H4+ + O2
      krate(icell,535) = kooij(6.80d-10,0d0,0d0,T)

      !O2+ + C2H6 -> C2H6+ + O2
      krate(icell,536) = kooij(1.21d-9,0d0,0d0,T)

      !O2+ + H2O2 -> H2O2+ + O2
      krate(icell,537) = kooij(1.50d-9,0d0,0d0,T)

      !C2H4O+ + C2H4O -> C2H5O+ + C2H3O
      krate(icell,538) = kooij(2.95d-9,0.00d+0,0.00d+0,T)

      !O2+ + e- -> O + O
      krate(icell,539) = kooij(1.95d-7,-7.00d-1,0.00d+0,T)

      !H2O+ + e- -> O + H2
      krate(icell,540) = kooij(3.90d-8,-5.00d-1,0.00d+0,T)

      !H2O+ + e- -> HO + H
      krate(icell,541) = kooij(8.60d-8,-5.00d-1,0.00d+0,T)

      !H2O+ + e- -> O + H + H
      krate(icell,542) = kooij(3.05d-7,-5.00d-1,0.00d+0,T)

      !CO+ + e- -> C + O
      krate(icell,543) = kooij(2.750d-7,-5.5d-1,0d0,T)

      !CHO+ + e- -> H + CO
      krate(icell,544) = kooij(2.800d-7,-6.900d-1,0d0,T)

      !CH2+ + e- -> C + H2
      krate(icell,545) = kooij(7.700d-8,-6.000d-1,0.0d0,T)

      !CH2+ + e- -> H + CH
      krate(icell,546) = kooij(1.600d-7,-6.000d-1,0.0d0,T)

      !CH2+ + e- -> C + H + H
      krate(icell,547) = kooij(4.000d-7,-6.000d-1,0.0d0,T)

      !H2+ + e- -> H + H
      krate(icell,548) = kooij(2.530d-7,-5.000d-1,0.0d0,T)

      !CH3+ + e- -> C + H + H2
      krate(icell,549) = kooij(3.000d-7,-3.000d-1,0.0d0,T)

      !CH3+ + e- -> H + H + CH
      krate(icell,550) = kooij(1.600d-7,-3.000d-1,0.0d0,T)

      !CH3+ + e- -> CH + H2
      krate(icell,551) = kooij(1.400d-7,-3.000d-1,0.0d0,T)

      !CH3+ + e- -> H + CH2
      krate(icell,552) = kooij(4.000d-7,-3.000d-1,0.0d0,T)

      !CH+ + e- -> C + H
      krate(icell,553) = kooij(7.000d-8,-5.000d-1,0.0d0,T)

      !HO+ + e- -> H + O
      krate(icell,554) = kooij(6.300d-9,-4.800d-1,0.0d0,T)

      !C2H5+ + e- -> C2H + H2 + H2
      krate(icell,555) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H5+ + e- -> C2H2 + H2 + H
      krate(icell,556) = kooij(3.00d-7,-5.00d-1,0.00d+0,T)

      !C2H5+ + e- -> C2H3 + H2
      krate(icell,557) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H5+ + e- -> C2H4 + H
      krate(icell,558) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H2+ + e- -> C2H + H
      krate(icell,559) = kooij(2.90d-7,-5.00d-1,0.00d+0,T)

      !C2H2+ + e- -> C2 + H + H
      krate(icell,560) = kooij(1.70d-7,-5.00d-1,0.00d+0,T)

      !C2H2+ + e- -> CH + CH
      krate(icell,561) = kooij(7.50d-8,-5.00d-1,0.00d+0,T)

      !C2H2+ + e- -> CH2 + C
      krate(icell,562) = kooij(2.89d-8,-5.00d-1,0.00d+0,T)

      !C2H2+ + e- -> C2 + H2
      krate(icell,563) = kooij(1.15d-8,-5.00d-1,0.00d+0,T)

      !C2HO+ + e- -> CO + C + H
      krate(icell,564) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2HO+ + e- -> CO + CH
      krate(icell,565) = kooij(1.00d-7,-5.00d-1,0.00d+0,T)

      !C2HO+ + e- -> C2H + O
      krate(icell,566) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2HO+ + e- -> C2O + H
      krate(icell,567) = kooij(1.00d-7,-5.00d-1,0.00d+0,T)

      !C2H3+ + e- -> C2H + H + H
      krate(icell,568) = kooij(2.95d-7,-8.40d-1,0.00d+0,T)

      !C2H3+ + e- -> C2H + H2
      krate(icell,569) = kooij(3.00d-8,-8.40d-1,0.00d+0,T)

      !C2H3+ + e- -> C2H2 + H
      krate(icell,570) = kooij(1.45d-7,-8.40d-1,0.00d+0,T)

      !C2H3+ + e- -> C2 + H + H2
      krate(icell,571) = kooij(1.50d-8,-8.40d-1,0.00d+0,T)

      !C2H3+ + e- -> CH3 + C
      krate(icell,572) = kooij(3.00d-9,-8.40d-1,0.00d+0,T)

      !C2H3+ + e- -> CH2 + CH
      krate(icell,573) = kooij(1.50d-8,-8.40d-1,0.00d+0,T)

      !C3H2+ + e- -> C2H2 + C
      krate(icell,574) = kooij(3.00d-8,-5.00d-1,0.00d+0,T)

      !CH3O+ + e- -> CO + H + H2
      krate(icell,575) = kooij(2.00d-7,-5.00d-1,0.00d+0,T)

      !CH3O+ + e- -> CHO + H + H
      krate(icell,576) = kooij(2.00d-7,-5.00d-1,0.00d+0,T)

      !CH3O+ + e- -> CH2O + H
      krate(icell,577) = kooij(2.00d-7,-5.00d-1,0.00d+0,T)

      !C2H4+ + e- -> C2H2 + H + H
      krate(icell,578) = kooij(3.70d-7,-7.60d-1,0.00d+0,T)

      !C2H4+ + e- -> C2H2 + H2
      krate(icell,579) = kooij(3.36d-8,-7.60d-1,0.00d+0,T)

      !C2H4+ + e- -> C2H3 + H
      krate(icell,580) = kooij(6.16d-8,-7.60d-1,0.00d+0,T)

      !C2H4+ + e- -> C2H + H2 + H
      krate(icell,581) = kooij(5.60d-8,-7.60d-1,0.00d+0,T)

      !C2H4+ + e- -> CH4 + C
      krate(icell,582) = kooij(5.60d-9,-7.60d-1,0.00d+0,T)

      !C2H4+ + e- -> CH3 + CH
      krate(icell,583) = kooij(1.12d-8,-7.60d-1,0.00d+0,T)

      !C2H4+ + e- -> CH2 + CH2
      krate(icell,584) = kooij(2.24d-8,-7.60d-1,0.00d+0,T)

      !C3H3+ + e- -> C2H2 + CH
      krate(icell,585) = kooij(6.99d-8,-5.00d-1,0.00d+0,T)

      !CO2+ + e- -> O + CO
      krate(icell,586) = kooij(4.200d-7,-7.500d-1,0d0,T)

      !C4H2+ + e- -> C2H + C2H
      krate(icell,587) = kooij(2.75d-7,-7.90d-1,0.00d+0,T)

      !C4H2+ + e- -> H + C4H
      krate(icell,588) = kooij(1.500d-7,-5.000d-1,0d0,T)

      !CH4+ + e- -> H + H + CH2
      krate(icell,589) = kooij(3.000d-7,-5.000d-1,0d0,T)

      !CH4+ + e- -> H + CH3
      krate(icell,590) = kooij(3.000d-7,-5.000d-1,0d0,T)

      !C2H+ + e- -> H + C2
      krate(icell,591) = kooij(1.160d-7,-7.600d-1,0d0,T)

      !C2H+ + e- -> C + CH
      krate(icell,592) = kooij(1.050d-7,-7.600d-1,0d0,T)

      !C2H+ + e- -> H + C + C
      krate(icell,593) = kooij(4.800d-8,-7.600d-1,0d0,T)

      !C3H7O+ + e- -> C2H4O + CH3
      krate(icell,594) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !CH3O2+ + e- -> CO2 + H2 + H
      krate(icell,595) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !CH3O2+ + e- -> CH2O2 + H
      krate(icell,596) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C4H5+ + e- -> H2 + H2 + C4H
      krate(icell,597) = kooij(1.01d-7,-5.0d-1,0d0,T)

      !C4H5+ + e- -> H + H2 + C4H2
      krate(icell,598) = kooij(1.01d-7,-5.0d-1,0d0,T)

      !C4H5+ + e- -> CH + C3H4
      krate(icell,599) = kooij(4.5d-8,-5.0d-1,0d0,T)

      !C4H5+ + e- -> C2H2 + C2H3
      krate(icell,600) = kooij(1.01d-7,-5.0d-1,0d0,T)

      !C4H5+ + e- -> C2H + C2H4
      krate(icell,601) = kooij(1.01d-7,-5.0d-1,0d0,T)

      !C2H4O+ + e- -> CH3 + CHO
      krate(icell,602) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H4O+ + e- -> C2H2O + H + H
      krate(icell,603) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H4O+ + e- -> C2H2O + H2
      krate(icell,604) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H5O+ + e- -> CH2O + CH3
      krate(icell,605) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H5O+ + e- -> C2H2O + H2 + H
      krate(icell,606) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H5O+ + e- -> CH4 + CO + H
      krate(icell,607) = kooij(3.00d-7,-5.00d-1,0.00d+0,T)

      !C2H5O+ + e- -> C2H4O + H
      krate(icell,608) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H7+ + e- -> C2H5 + H2
      krate(icell,609) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H7+ + e- -> C2H6 + H
      krate(icell,610) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H2O+ + e- -> C2 + H2O
      krate(icell,611) = kooij(2.00d-7,-5.00d-1,0.00d+0,T)

      !C2H2O+ + e- -> CH2 + CO
      krate(icell,612) = kooij(2.00d-7,-5.00d-1,0.00d+0,T)

      !C2H2O+ + e- -> C2H2 + O
      krate(icell,613) = kooij(2.00d-7,-5.00d-1,0.00d+0,T)

      !C2H3O+ + e- -> CH3 + CO
      krate(icell,614) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H3O+ + e- -> C2H2O + H
      krate(icell,615) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2+ + e- -> C + C
      krate(icell,616) = kooij(8.84d-8,-5.00d-1,0.00d+0,T)

      !H3+ + e- -> H + H + H
      krate(icell,617) = kooij(4.36d-8,-5.20d-1,0.00d+0,T)

      !H3+ + e- -> H2 + H
      krate(icell,618) = kooij(2.34d-8,-5.20d-1,0.00d+0,T)

      !H3O+ + e- -> HO + H + H
      krate(icell,619) = kooij(2.60d-7,-5.00d-1,0.00d+0,T)

      !H3O+ + e- -> H2O + H
      krate(icell,620) = kooij(1.10d-7,-5.00d-1,0.00d+0,T)

      !H3O+ + e- -> HO + H2
      krate(icell,621) = kooij(6.00d-8,-5.00d-1,0.00d+0,T)

      !H3O+ + e- -> H2 + H + O
      krate(icell,622) = kooij(5.60d-9,-5.00d-1,0.00d+0,T)

      !CH2O+ + e- -> CO + H + H
      krate(icell,623) = kooij(5.00d-7,-5.00d-1,0.00d+0,T)

      !CH2O+ + e- -> CHO + H
      krate(icell,624) = kooij(1.00d-7,-5.00d-1,0.00d+0,T)

      !CH4O+ + e- -> CH3 + HO
      krate(icell,625) = kooij(3.00d-7,-5.00d-1,0.00d+0,T)

      !CH4O+ + e- -> CH2O + H2
      krate(icell,626) = kooij(3.00d-7,-5.00d-1,0.00d+0,T)

      !CH5+ + e- -> CH3 + H2
      krate(icell,627) = kooij(1.40d-8,-5.20d-1,0.00d+0,T)

      !CH5+ + e- -> CH4 + H
      krate(icell,628) = kooij(1.40d-8,-5.20d-1,0.00d+0,T)

      !CH5+ + e- -> CH3 + H + H
      krate(icell,629) = kooij(1.95d-7,-5.20d-1,0.00d+0,T)

      !CH5+ + e- -> CH2 + H2 + H
      krate(icell,630) = kooij(4.80d-8,-5.20d-1,0.00d+0,T)

      !CH5+ + e- -> CH + H2 + H2
      krate(icell,631) = kooij(3.00d-9,-5.20d-1,0.00d+0,T)

      !C3H4+ + e- -> C3H3 + H
      krate(icell,632) = kooij(2.57d-6,-6.70d-1,0.00d+0,T)

      !C3H4+ + e- -> C2H3 + CH
      krate(icell,633) = kooij(2.95d-8,-6.70d-1,0.00d+0,T)

      !C3H4+ + e- -> C2H2 + CH2
      krate(icell,634) = kooij(1.77d-7,-6.70d-1,0.00d+0,T)

      !C3H4+ + e- -> C2H + CH3
      krate(icell,635) = kooij(2.95d-8,-6.70d-1,0.00d+0,T)

      !CH5O+ + e- -> CH2O + H2 + H
      krate(icell,636) = kooij(9.10d-8,-6.70d-1,0.00d+0,T)

      !CH5O+ + e- -> CH3 + H2O
      krate(icell,637) = kooij(8.19d-8,-6.70d-1,0.00d+0,T)

      !CH5O+ + e- -> CH3 + HO + H
      krate(icell,638) = kooij(4.64d-7,-6.70d-1,0.00d+0,T)

      !CH5O+ + e- -> CH2 + H2O + H
      krate(icell,639) = kooij(1.91d-7,-6.70d-1,0.00d+0,T)

      !CH5O+ + e- -> CH3OH + H
      krate(icell,640) = kooij(2.73d-8,-6.70d-1,0.00d+0,T)

      !C2H6+ + e- -> C2H4 + H2
      krate(icell,641) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H6+ + e- -> C2H5 + H
      krate(icell,642) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H7O+ + e- -> C2H4 + H2O + H
      krate(icell,643) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H7O+ + e- -> C2H4O + H2 + H
      krate(icell,644) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !C2H7O+ + e- -> C2H5OH + H
      krate(icell,645) = kooij(1.50d-7,-5.00d-1,0.00d+0,T)

      !HO2+ + e- -> O2 + H
      krate(icell,646) = kooij(3.00d-7,-5.00d-1,0.00d+0,T)

      !CHO2+ + e- -> CO + H + O
      krate(icell,647) = kooij(8.10d-7,-6.40d-1,0.00d+0,T)

      !CHO2+ + e- -> HO + CO
      krate(icell,648) = kooij(3.20d-7,-6.40d-1,0.00d+0,T)

      !CHO2+ + e- -> CO2 + H
      krate(icell,649) = kooij(6.00d-8,-6.40d-1,0.00d+0,T)

      !C+ + e- -> C
      krate(icell,650) = kooij(4.40d-12,-6.10d-1,0.0d0,T)

      !H+ + e- -> H
      krate(icell,651) = kooij(3.5d-12,-7.5d-1,0d0,T)

      !O+ + e- -> O
      krate(icell,652) = kooij(3.4d-12,-6.3d-1,0d0,T)

      !H2+ + e- -> H2
      krate(icell,653) = kooij(2.250d-7,-4.000d-1,0.0d0,T)

      !CH3+ + e- -> CH3
      krate(icell,654) = kooij(1.100d-10,-7.000d-1,0.0d0,T)

      !C -> C+ + e-
      krate(icell,655) = 1.02d+3 * cr_flux(icell)

      !H -> H+ + e-
      krate(icell,656) = 4.6d-1 * cr_flux(icell)

      !O -> O+ + e-
      krate(icell,657) = 2.8d0 * cr_flux(icell)

      !C2 -> C + C
      krate(icell,658) = 1.3d+3 * cr_flux(icell)

      !CH -> C + H
      krate(icell,659) = 7.3d+2 * cr_flux(icell)

      !CO -> C + O
      krate(icell,660) = 5.0d0 * cr_flux(icell)

      !CO -> CO+ + e-
      krate(icell,661) = 3.0d0 * cr_flux(icell)

      !H2 -> H+ + H + e-
      krate(icell,662) = 2.2d-2 * cr_flux(icell)

      !H2 -> H2+ + e-
      krate(icell,663) = 9.3d-1 * cr_flux(icell)

      !H2 -> H + H
      krate(icell,664) = 1.0d-1 * cr_flux(icell)

      !O2 -> O + O
      krate(icell,665) = 7.5d+2 * cr_flux(icell)

      !O2 -> O2+ + e-
      krate(icell,666) = 1.170d+2 * cr_flux(icell)

      !HO -> O + H
      krate(icell,667) = 5.1d+2 * cr_flux(icell)

      !C2H -> C2 + H
      krate(icell,668) = 5.0d+3 * cr_flux(icell)

      !CH2 -> CH2+ + e-
      krate(icell,669) = 5.0d+2 * cr_flux(icell)

      !CO2 -> CO + O
      krate(icell,670) = 1.71d+3 * cr_flux(icell)

      !H2O -> HO + H
      krate(icell,671) = 7.6d+2 * cr_flux(icell)

      !CHO -> CO + H
      krate(icell,672) = 4.21d+2 * cr_flux(icell)

      !CHO -> CHO+ + e-
      krate(icell,673) = 1.17d+3 * cr_flux(icell)

      !HO2 -> O + HO
      krate(icell,674) = 7.5d+2 * cr_flux(icell)

      !HO2 -> O2 + H
      krate(icell,675) = 7.5d+2 * cr_flux(icell)

      !C2H2 -> C2H + H
      krate(icell,676) = 5.15d+3 * cr_flux(icell)

      !C2H2 -> C2H2+ + e-
      krate(icell,677) = 1.31d+3 * cr_flux(icell)

      !CH3 -> CH2 + H
      krate(icell,678) = 5.0d+2 * cr_flux(icell)

      !CH3 -> CH3+ + e-
      krate(icell,679) = 5.0d+2 * cr_flux(icell)

      !CH2O -> CO + H2
      krate(icell,680) = 2.66d+3 * cr_flux(icell)

      !H2O2 -> HO + HO
      krate(icell,681) = 1.5d+3 * cr_flux(icell)

      !CH4 -> CH2 + H2
      krate(icell,682) = 2.34d+3 * cr_flux(icell)

      !C2H4 -> C2H2 + H2
      krate(icell,683) = 3.7d+3 * cr_flux(icell)

      !C2H4 -> C2H4+ + e-
      krate(icell,684) = 2.23d+3 * cr_flux(icell)

      !CH3OH -> CH3O+ + H + e-
      krate(icell,685) = 2.25d+2 * cr_flux(icell)

      !C2H5 -> C2H4 + H
      krate(icell,686) = 1.5d+3 * cr_flux(icell)

      !CH+ -> C + H+
      krate(icell,687) = 6.4d+1 * cr_flux(icell)

      !O3 -> O + O2
      krate(icell,688) = 1.49d+3 * cr_flux(icell)

      !CH3COCH3 -> C2H2O + CH4
      krate(icell,689) = 1.5d+3 * cr_flux(icell)

      !C4H4 -> C + C3H4
      krate(icell,690) = 2.00d+3 * cr_flux(icell)

      !CH+ -> H + C+
      krate(icell,691) = 1.12d+2 * cr_flux(icell)

      !CH2 -> C + H2
      krate(icell,692) = 2.9d+2 * cr_flux(icell)

      !H2O -> O + H2
      krate(icell,693) = 2.2d+2 * cr_flux(icell)

      !HCOOH -> HO + CHO
      krate(icell,694) = 2.49d+2 * cr_flux(icell)

      !C4H2 -> C2H + C2H
      krate(icell,695) = 1.73d+3 * cr_flux(icell)

      !C4H2 -> C4H + H
      krate(icell,696) = 1.73d+3 * cr_flux(icell)

      !C4H2 -> C4H2+ + e-
      krate(icell,697) = 1.73d+3 * cr_flux(icell)

      !CH3OH -> CH3 + HO
      krate(icell,698) = 5.14d+2 * cr_flux(icell)

      !CH3OH -> CH2O + H2
      krate(icell,699) = 1.09d+3 * cr_flux(icell)

      !C2H4O -> CHO + CH3
      krate(icell,700) = 1.12d+3 * cr_flux(icell)

      !C2H4O -> CO + CH4
      krate(icell,701) = 1.12d+3 * cr_flux(icell)

      !C2H4O -> C2H4O+ + e-
      krate(icell,702) = 6.92d+2 * cr_flux(icell)

      !C3H4 -> C3H3 + H
      krate(icell,703) = 3.28d+3 * cr_flux(icell)

      !C3H4 -> C3H4+ + e-
      krate(icell,704) = 5.3d+3 * cr_flux(icell)

      !C3H4 -> CH2 + C2H2
      krate(icell,705) = 2.0d+3 * cr_flux(icell)

      !C4H3 -> C4H2 + H
      krate(icell,706) = 1.0d+4 * cr_flux(icell)

      !C2H6 -> C2H4 + H2
      krate(icell,707) = 2.06d+3 * cr_flux(icell)

      !C2H6 -> C2H6+ + e-
      krate(icell,708) = 1.78d+2 * cr_flux(icell)

      !HO+ -> H + O+
      krate(icell,709) = 8.57d0 * cr_flux(icell)

      !O2+ -> O + O+
      krate(icell,710) = 6.97d+1 * cr_flux(icell)

      !CH4 -> CH4+ + e-
      krate(icell,711) = 2.24d+1 * cr_flux(icell)

      !C2H2O -> CO + CH2
      krate(icell,712) = 9.15d+2 * cr_flux(icell)

      !C2H2O -> C2H2O+ + e-
      krate(icell,713) = 1.22d+3 * cr_flux(icell)

      !C2 -> C2+ + e-
      krate(icell,714) = 2.46d+2 * cr_flux(icell)

      !C2H6+ + H -> C2H5+ + H2
      krate(icell,715) = kooij(1.00d-10,0.00d+0,0.00d+0,T)

      !O + CH2 -> H + CHO
      krate(icell,716) = kooij(2.00d-12,0.00d+0,0.00d+0,T)

      !CO + HO -> H + CO2
      krate(icell,717) = kooij(2.81d-13,0.00d+0,1.76d+2,T)

      !HO + HO -> O + H2O
      krate(icell,718) = kooij(1.65d-12,1.14d+0,5.00d+1,T)

      !HO + HO -> O + H2O
      krate(icell,719) = kooij(6.20d-14,2.62d+0,-9.45d+2,T)

      !CH2O + HO -> CHO + H2O
      krate(icell,720) = kooij(1.00d-11,0.00d+0,0.00d+0,T)

      !CHO+ + C -> CH+ + CO
      krate(icell,721) = kooij(1.10d-9,0.00d+0,0.00d+0,T)

      !CHO+ + CH -> CH2+ + CO
      krate(icell,722) = ionpol1(1.00d+0,1.14d-9,3.33d+0,T)

      !CHO+ + HO -> H2O+ + CO
      krate(icell,723) = ionpol1(5.00d-1,7.42d-10,5.50d+0,T)

      !CH5+ + H -> CH4+ + H2
      krate(icell,724) = kooij(1.50d-10,0.00d+0,4.81d+2,T)

      !CH5+ + CH2 -> CH3+ + CH4
      krate(icell,725) = ionpol1(1.00d+0,1.23d-9,1.41d+0,T)

      !CH5+ + CH2 -> CH3+ + CH4
      krate(icell,726) = ionpol2(1.00d+0,1.23d-9,1.41d+0,T)

      !C2H5+ + H2O -> H3O+ + C2H4
      krate(icell,727) = ionpol1(1.00d+0,8.34d-10,5.41d+0,T)

      !CO + C -> O + C2
      krate(icell,728) = kooij(1.00d-10,0.00d+0,5.28d+4,T)

      !C + H2 -> CH + H
      krate(icell,729) = kooij(6.64d-10,0.00d+0,1.17d+4,T)

      !H2 + CH -> H + CH2
      krate(icell,730) = kooij(3.75d-10,0.00d+0,1.66d+3,T)

      !H2 + CH2 -> H + CH3
      krate(icell,731) = kooij(5.00d-11,0.00d+0,4.87d+3,T)

      !H2 + CH3 -> H + CH4
      krate(icell,732) = kooij(2.51d-13,0.00d+0,4.21d+3,T)

      !H2 + O -> H + HO
      krate(icell,733) = kooij(3.44d-13,2.67d+0,3.16d+3,T)

      !H2 + HO -> H + H2O
      krate(icell,734) = kooij(8.40d-13,0.00d+0,1.04d+3,T)

      !H2 + HO2 -> H + H2O2
      krate(icell,735) = kooij(4.38d-12,0.00d+0,1.08d+4,T)

      !HO + O -> H + O2
      krate(icell,736) = kooij(4.00d-11,0.00d+0,0.00d+0,T)

      !HO + O -> H + O2
      krate(icell,737) = kooij(3.50d-11,0.00d+0,0.00d+0,T)

      !HO + O -> H + O2
      krate(icell,738) = kooij(2.40d-11,0.00d+0,-1.10d+2,T)

      !H3+ + O2 -> HO2+ + H2
      krate(icell,739) = kooij(6.40d-10,0.00d+0,0.00d+0,T)

      !CH4+ + H -> CH3+ + H2
      krate(icell,740) = kooij(1.00d-11,0.00d+0,0.00d+0,T)

      !C2H4+ + H -> C2H3+ + H2
      krate(icell,741) = kooij(3.00d-10,0.00d+0,0.00d+0,T)

      !CHO+ + C -> CH+ + CO
      krate(icell,742) = kooij(1.40d-9,0.00d+0,0.00d+0,T)

      !CHO+ + CH -> CH2+ + CO
      krate(icell,743) = kooij(9.00d-9,-5.00d-1,0.00d+0,T)

      !H3O+ + CH -> CH2+ + H2O
      krate(icell,744) = ionpol1(1.00d+0,1.23d-9,3.33d+0,T)

      !H3O+ + CH2O -> CH3O+ + H2O
      krate(icell,745) = ionpol1(1.00d+0,1.11d-9,5.15d+0,T)

      !CHO2+ + CO -> CHO+ + CO2
      krate(icell,746) = ionpol2(1.00d+0,7.86d-10,2.51d-1,T)

      !CHO2+ + CH4 -> CH5+ + CO2
      krate(icell,747) = kooij(7.80d-10,0.00d+0,0.00d+0,T)

      !C2H3+ + H -> C2H2+ + H2
      krate(icell,748) = kooij(6.80d-11,0.00d+0,0.00d+0,T)

      !C2H3+ + C2H -> C2H2+ + C2H2
      krate(icell,749) = ionpol1(3.33d-1,1.36d-9,1.34d+0,T)

      !C2H3+ + C2H -> C2H2+ + C2H2
      krate(icell,750) = ionpol2(3.33d-1,1.36d-9,1.34d+0,T)

      !CH+ + H -> C+ + H2
      krate(icell,751) = kooij(7.50d-10,0.00d+0,0.00d+0,T)

      !HO2+ + CO -> CHO+ + O2
      krate(icell,752) = ionpol2(1.00d+0,8.42d-10,2.51d-1,T)

      !HO2+ + H2 -> H3+ + O2
      krate(icell,753) = kooij(3.20d-10,0.00d+0,0.00d+0,T)

      !HO + C -> O + CH
      krate(icell,754) = kooij(2.25d-11,5.00d-1,1.48d+4,T)

      !CH4 + CH2 -> CH3 + CH3
      krate(icell,755) = kooij(7.13d-12,0.00d+0,5.05d+3,T)

      !CH3 + CH3 -> H + C2H5
      krate(icell,756) = kooij(1.46d-11,1.00d-1,5.34d+3,T)

      !H2 + C2 -> H + C2H
      krate(icell,757) = kooij(1.60d-10,0.00d+0,1.42d+3,T)

      !H2 + C2H -> H + C2H2
      krate(icell,758) = kooij(1.14d-11,0.00d+0,1.30d+3,T)

      !H2 + C2H3 -> H + C2H4
      krate(icell,759) = kooij(1.61d-13,2.63d+0,4.29d+3,T)

      !C2H5 + H2 -> H + C2H6
      krate(icell,760) = kooij(4.12d-15,3.60d+0,4.24d+3,T)

      !C2H2 + O -> HO + C2H
      krate(icell,761) = kooij(5.30d-9,0.00d+0,8.52d+3,T)

      !CH3 + HO -> O + CH4
      krate(icell,762) = kooij(3.27d-14,2.20d+0,2.24d+3,T)

      !C2H2 + HO -> CO + CH3
      krate(icell,763) = kooij(3.76d-16,0.00d+0,0.00d+0,T)

      !CH3 + H2O -> HO + CH4
      krate(icell,764) = kooij(2.30d-15,3.47d+0,6.68d+3,T)

      !CH2O + CH3 -> CHO + CH4
      krate(icell,765) = kooij(1.34d-15,5.05d+0,1.64d+3,T)

      !HO2 + HO2 -> O2 + H2O2
      krate(icell,766) = kooij(5.64d-12,0.00d+0,0.00d+0,T)

      !CH2+ + H -> CH+ + H2
      krate(icell,767) = kooij(1.20d-9,0.00d+0,2.70d+3,T)

      !CH3+ + H -> CH2+ + H2
      krate(icell,768) = kooij(7.00d-10,0.00d+0,1.06d+4,T)

      !H3+ + H -> H2+ + H2
      krate(icell,769) = kooij(2.10d-9,0.00d+0,2.00d+4,T)

      !H3O+ + H -> H2O+ + H2
      krate(icell,770) = kooij(6.10d-10,0.00d+0,2.05d+4,T)

      !CHO+ + H -> CO+ + H2
      krate(icell,771) = kooij(1.30d-9,0.00d+0,2.45d+4,T)

      !CHO+ + CH3 -> CH4+ + CO
      krate(icell,772) = kooij(1.40d-9,0.00d+0,9.06d+3,T)

      !CHO+ + CH4 -> CH5+ + CO
      krate(icell,773) = kooij(9.90d-10,0.00d+0,4.92d+3,T)

      !CH3+ + H -> CH2+ + H2
      krate(icell,774) = kooij(7.00d-10,0.00d+0,7.00d+2,T)

      !CH3 + HO2 -> O2 + CH4
      krate(icell,775) = kooij(6.00d-12,0.00d+0,0.00d+0,T)

      !CO + O2 -> O + CO2
      krate(icell,776) = kooij(5.99d-12,0.00d+0,2.41d+4,T)

      !H2 + O2 -> H + HO2
      krate(icell,777) = kooij(2.40d-10,0.00d+0,2.85d+4,T)

      !CH2O + HO2 -> CHO + H2O2
      krate(icell,778) = kooij(3.30d-12,0.00d+0,5.87d+3,T)

      !HO2 + H2O -> HO + H2O2
      krate(icell,779) = kooij(4.65d-11,0.00d+0,1.65d+4,T)

      !CHO + HO2 -> O2 + CH2O
      krate(icell,780) = kooij(5.00d-11,0.00d+0,0.00d+0,T)

      !H2O + O -> H + HO2
      krate(icell,781) = kooij(4.48d-12,9.70d-1,3.45d+4,T)

      !C + H2 -> CH + H
      krate(icell,782) = kooij(1.60d-10,0.00d+0,1.42d+3,T)

      !C4H2 + H -> C4H + H2
      krate(icell,783) = kooij(3.80d-10,0.00d+0,1.36d+4,T)

      !C4H2 + H -> C2H + C2H2
      krate(icell,784) = kooij(9.96d-10,0.00d+0,7.75d+3,T)

      !C2H2 + H2 -> H + C2H3
      krate(icell,785) = kooij(4.00d-12,0.00d+0,3.26d+4,T)

      !C2H4 + H2 -> H + C2H5
      krate(icell,786) = kooij(1.69d-11,0.00d+0,3.42d+4,T)

      !H2 + CH -> H + CH2
      krate(icell,787) = kooij(1.14d-11,0.00d+0,9.50d+2,T)

      !O+ + H -> H+ + O
      krate(icell,788) = kooij(7.00d-10,0.00d+0,0.00d+0,T)

      !CO2+ + O -> O+ + CO2
      krate(icell,789) = kooij(9.62d-11,0.00d+0,0.00d+0,T)

      !H+ + H2 -> H2+ + H
      krate(icell,790) = kooij(6.40d-10,0.00d+0,2.13d+4,T)

      !!$       kmax = 1d-2
      !!$       if(icell==1) print '(2a7,2a17)', "icell", "irate", "Tgas", "krate"
      !!$       do i=1,size(krate,2)
      !!$          if(krate(icell,i)>kmax) then
      !!$             print '(2I7,2E17.8e3)', i, icell, T, krate(icell,i)
      !!$          end if
      !!$       end do
    end do

  end subroutine computeRates

end module patmo_rates
