module patmo_hydrostatic
  contains

  ! **************************
  ! compute hydrostatic equilibrium pressure.
  ! this method assumes that dp(z)/dz = p(z)*f(z)
  ! where f(z) = -mu(z)*mp*g/kb/T(z)
  ! can be solved as
  ! p(z) = p0 * exp(int_0^z f(x) dx)
  ! https://www.wolframalpha.com/input/?i=solve+f%27+%3D+f+*+g(x)
  subroutine set_hydrostatic_pressure(p0)
    use patmo_commons
    use patmo_parameters
    use patmo_constants
    use patmo_utils
    implicit none
    real*8,intent(in)::p0
    real*8::fint, f1, f2, n(speciesNumber), dz
    integer::i, j

    ! pressure at ground
    pressure(1) = p0

    ! loop on cells
    do i=2,cellsNumber
       fint = 0d0
       ! do integral trapezoid
       do j=2,i
          ! evaluate first point
          n(:) = nall(j-1,:)
          f1 = getMeanMass(n(:)) * gravity / kboltzmann / TgasAll(j-1)
          ! evaluate second point
          n(:) = nall(j,:)
          f2 = getMeanMass(n(:)) * gravity / kboltzmann / TgasAll(j)
          ! differential
          dz = height(j) - height(j-1)
          ! integrate
          fint = fint + (f1 + f2) * dz / 2d0
       end do

       ! compute pressure
       pressure(i) = p0 * exp(-fint)

    end do

  end subroutine set_hydrostatic_pressure

end module patmo_hydrostatic
